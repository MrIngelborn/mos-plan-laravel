# MoS PLAN Portal

## Requirements
- composer
- php
- (php-sqlite3)
- php-curl

## Installation

Copy `.env.example` to `.env` and modify it as needed.
Then run the following commands:

```bash
composer install
php artisan key:generate
php artisan migrate
php artisan db:seed
```
