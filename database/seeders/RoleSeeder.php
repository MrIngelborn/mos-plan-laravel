<?php
namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'permissions' => [
                '*' => true,
            ]
        ]);
        $shop = Role::create([
            'name' => 'Shop',
            'slug' => 'shop',
            'permissions' => [
                'create_purchase' => true,
            ]
        ]);
    }
}
