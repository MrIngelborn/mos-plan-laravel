<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<Event>
 */
class EventFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->word,
            'entry_price' => 0,
            'start_time' => fake()->dateTime,
            'end_time' => fake()->dateTime,
            'barcode_prefix' => fake()->word,
            'uri' => fake()->word,
        ];
    }
}
