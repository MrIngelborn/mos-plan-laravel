<?php

namespace Database\Factories\Event\Registration;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class FormInputFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->word,
            'label' => fake()->word,
            'type' => fake()->word,
            'placeholder' => fake()->word,
            'limit' => 0,
            'order' => fake()->numberBetween(0, 100),
            'oncard' => fake()->boolean(),
        ];
    }
}
