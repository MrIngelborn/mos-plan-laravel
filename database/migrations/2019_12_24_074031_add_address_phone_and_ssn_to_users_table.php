<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressPhoneAndSsnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('birthdate');
            $table->string('ssn',12)->nullable();
            $table->string('phone1',15)->nullable();
            $table->string('phone2',15)->nullable();
            $table->string('street',50)->nullable();
            $table->string('zip_code',5)->nullable();
            $table->string('city',20)->nullable();
            $table->date('last_renewed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('birthdate')->nullable();
            $table->dropColumn('ssn');
            $table->dropColumn('phone1');
            $table->dropColumn('phone2');
            $table->dropColumn('street');
            $table->dropColumn('zip_code');
            $table->dropColumn('city');
            $table->dropColumn('last_renewed');
        });
    }
}
