<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('event_id')->after('id');
            
            $table->dropUnique('products_barcode_unique');
            $table->unique(['event_id', 'barcode']);
            
            $table->foreign('event_id')
                ->references('id')->on('events')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropUnique('products_event_id_barcode_unique');
            $table->dropForeign(['event_id']);
            $table->dropColumn('event_id');
            
            $table->unique('barcode');
        });
    }
}
