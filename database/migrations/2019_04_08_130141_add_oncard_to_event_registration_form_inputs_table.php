<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOncardToEventRegistrationFormInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_registration_form_inputs', function (Blueprint $table) {
            $table->boolean('oncard')->after('limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_registration_form_inputs', function (Blueprint $table) {
            $table->dropColumn('oncard');
        });
    }
}
