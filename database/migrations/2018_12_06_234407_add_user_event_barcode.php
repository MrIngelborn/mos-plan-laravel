<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserEventBarcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_user', function (Blueprint $table) {
            $table->unsignedTinyInteger('barcode_id');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->string('barcode_prefix', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_user', function (Blueprint $table) {
            $table->dropColumn('barcode_id');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->dropColumn('barcode_prefix');
        });
    }
}
