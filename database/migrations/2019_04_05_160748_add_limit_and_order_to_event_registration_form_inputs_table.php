<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimitAndOrderToEventRegistrationFormInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_registration_form_inputs', function (Blueprint $table) {
            //
            $table->integer('limit')->unsigned()->after('placeholder');
            $table->integer('order')->unsigned()->after('limit');
            
            //$table->unique(['form_id', 'order']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_registration_form_inputs', function (Blueprint $table) {
            //
            //$table->dropUnique(['form_id', 'order']);
            
            $table->dropColumn('limit');
            $table->dropColumn('order');
        });
    }
}
