<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPurchaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_purchase', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('purchase_id');
            $table->unsignedTinyInteger('amount');
            
            $table->foreign('product_id')
                    ->references('id')->on('products')
                    ->onDelete('restrict');
            $table->foreign('purchase_id')
                    ->references('id')->on('purchases')
                    ->onDelete('cascade');
            
            $table->primary(['product_id', 'purchase_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_purchase');
    }
}
