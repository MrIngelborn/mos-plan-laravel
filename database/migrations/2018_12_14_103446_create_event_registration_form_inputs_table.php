<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRegistrationFormInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_registration_form_inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('form_id');
            $table->string('name', 100);
            $table->string('label', 100);
            $table->string('type', 100);
            $table->string('placeholder', 100);
            $table->timestamps();
            
            $table->unique(['form_id', 'name']);
            $table->foreign('form_id')->references('id')->on('event_registration_forms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_registration_form_inputs');
    }
}
