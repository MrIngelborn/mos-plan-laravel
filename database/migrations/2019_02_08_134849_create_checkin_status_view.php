<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinStatusView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Drop to make sure it is reset as Laravel don't drop views on refresh
        DB::statement($this->dropViewSQL());
        DB::statement($this->createViewSQL());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropViewSQL());
    }

    private function createViewSQL() : string
    {
        return <<<SQL
CREATE VIEW view_checkin_status AS
    SELECT checkin_events.*
    FROM checkin_events
    JOIN (
        SELECT user_id, event_id, MAX(time) AS lastone
        FROM checkin_events GROUP BY user_id, event_id
    ) checkin_events2
    ON checkin_events.user_id = checkin_events2.user_id
    AND checkin_events.time = checkin_events2.lastone

SQL;
    }

    private function dropViewSQL() : string
    {
        return <<<SQL
DROP VIEW IF EXISTS view_checkin_status;
SQL;
    }
}
