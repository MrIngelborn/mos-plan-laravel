<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRegistrationFormResponseValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_registration_form_response_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('response_id');
            $table->unsignedInteger('input_id');
            $table->text('value');
            $table->timestamps();
            
            $table->unique(['response_id', 'input_id'], 'event_form_response_values_response_input_unique');
            $table->foreign('response_id')->references('id')->on('event_registration_form_responses')->onDelete('cascade');
            $table->foreign('input_id')->references('id')->on('event_registration_form_inputs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_registration_form_response_values');
    }
}
