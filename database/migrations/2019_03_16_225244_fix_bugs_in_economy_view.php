<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBugsInEconomyView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Drop to make sure it is reset as Laravel don't drop views on refresh
        DB::statement($this->dropViewSQL());
        DB::statement($this->newViewSQL());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropViewSQL());
        DB::statement($this->oldViewSQL());
    }
    
    private function newViewSQL()
    {
        return <<<SQL
CREATE VIEW view_user_economy AS

SELECT 
    users.id AS user_id,
    events.id AS event_id,
    coalesce(events.entry_price,0) AS entry_price,
    coalesce(event_extra.sum,0) AS event_extra,
    coalesce(event_purchases.total_purchase,0) AS event_purchases,
    0 AS payed,
    (coalesce(events.entry_price,0) + coalesce(event_extra.sum,0) + coalesce(event_purchases.total_purchase,0) - 0) AS debt 
    
FROM users

LEFT JOIN event_user
ON users.id = event_user.user_id

LEFT JOIN events 
ON event_user.event_id = events.id

LEFT JOIN 
    (
        SELECT
            event_registration_form_responses.user_id AS user_id,
            event_registration_forms.event_id AS event_id,
            sum(event_registration_form_inputs.price) AS sum 
        
        FROM event_registration_form_response_values 
        
        LEFT JOIN event_registration_form_responses 
        ON event_registration_form_responses.id = event_registration_form_response_values.response_id
        
        LEFT JOIN event_registration_form_inputs 
        ON event_registration_form_inputs.id = event_registration_form_response_values.input_id
        
        LEFT JOIN event_registration_forms 
        ON event_registration_forms.id = event_registration_form_responses.form_id
        
        GROUP BY 
            event_registration_form_responses.user_id,
            event_registration_forms.event_id
    ) event_extra 
    ON 
        event_extra.event_id = events.id

    LEFT JOIN
    (
        SELECT 
            purchases.user_id AS user_id,
            purchases.event_id AS event_id,
            coalesce(sum((products.price * product_purchase.amount)),0) AS total_purchase 
        FROM 
            purchases 
            
            LEFT JOIN product_purchase 
            ON purchases.id = product_purchase.purchase_id 
            
            LEFT JOIN products 
            ON product_purchase.product_id = products.id
        
        GROUP BY
            purchases.user_id,
            purchases.event_id
    ) event_purchases 
    ON event_purchases.user_id = users.id
; 
SQL;
    }
    
    private function oldViewSQL() : string
    {
        return <<<SQL
CREATE VIEW view_user_economy AS

SELECT
	users.id AS user_id,
    events.id AS event_id,
    events.entry_price,
	prices.event_extra,
    prices.event_purchases,
	0 as payed,
    (	events.entry_price + 
    	prices.event_extra +
		prices.event_purchases
    ) - 0 as debt
FROM users
LEFT JOIN event_user
	ON users.id = event_user.user_id
LEFT JOIN events
	ON event_user.event_id = events.id
LEFT JOIN (
    SELECT
		event_prices.event_id AS event_id,
	    event_prices.optionals_price AS event_extra,
	    event_purchases.total_purchase AS event_purchases
    FROM (
		SELECT 
		    event_registration_form_responses.user_id as user_id,
		    event_registration_forms.event_id as event_id,
		    COALESCE(SUM(event_registration_form_inputs.price), 0) AS optionals_price
		FROM event_registration_form_responses
		LEFT JOIN event_registration_forms 
			ON event_registration_form_responses.form_id = event_registration_forms.id
		LEFT JOIN events 
			ON event_registration_forms.event_id = events.id
		LEFT JOIN event_registration_form_response_values
		    ON event_registration_form_responses.id = 
		    	event_registration_form_response_values.response_id
		LEFT JOIN event_registration_form_inputs
			ON event_registration_form_inputs.id = 
		    	event_registration_form_response_values.input_id
		GROUP BY user_id, event_id
	) AS event_prices
	JOIN
	(
	    SELECT 
		    purchases.user_id as user_id,
		    purchases.event_id AS event_id,
		    COALESCE(SUM(products.price * product_purchase.amount), 0) as total_purchase
		FROM purchases
		LEFT JOIN product_purchase 
			ON purchases.id = product_purchase.purchase_id
		LEFT JOIN products 
			ON product_purchase.product_id = products.id
		GROUP BY user_id, event_id
	) AS event_purchases
	ON event_prices.user_id = event_purchases.user_id
) AS prices
ON events.id = prices.event_id
SQL;
    }
    
    private function dropViewSQL() : string
    {
        return <<<SQL
DROP VIEW IF EXISTS view_user_economy;
SQL;
    }
}
