<?php

return [
    'form' => 'Registration form',
    'form.for' => 'Form for :name',
    'form.create' => 'Create new form',
    'form.edit' => 'Edit form',
    'form.registration_opens_at' => 'Registration opens at',
    'form.registration_closes_at' => 'Registration closes at',
    'form.inputs' => 'Form inputs',
    'form.missing' => 'No form exists yet.',
    'form.input.name' => 'Name',
    'form.input.label' => 'Label',
    'form.input.type' => 'Type',
    'form.input.placeholder' => 'Placeholder',
    'form.input.price' => 'Price',
    'form.input.limit' => 'Limit',
    'form.input.order' => 'Order',
    'form.input.oncard' => 'On Card',
    'form.input.create' => 'Create input',
];
