<?php

return [
    'form' => 'Formulär',
    'form.for' => ':name\'s formulär',
    'form.create' => 'Skapa nytt formulär',
    'form.edit' => 'Redigera formulär',
    'form.registration_opens_at' => 'Registrering öppnar',
    'form.registration_closes_at' => 'Registrering stänger',
    'form.inputs' => 'Formulärsfält',
    'form.missing' => 'Inget formulär existerar än.',
    'form.input.name' => 'Namn',
    'form.input.label' => 'Etikett',
    'form.input.type' => 'Typ',
    'form.input.placeholder' => 'Platshållare',
    'form.input.price' => 'Pris',
    'form.input.limit' => 'Begränsning',
    'form.input.order' => 'Ordning',
    'form.input.oncard' => 'På kortet',
    'form.input.create' => 'Skapa nytt fält',
];
