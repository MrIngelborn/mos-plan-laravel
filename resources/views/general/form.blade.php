@extends('layouts.app')

@section('content_class', 'form')

@section('content')
    <h1>{{ $title }}</h1>

    <form
        action="{{ isset($form->action) ? $form->action : '' }}"
        method="POST"
    >
        @csrf
        @isset($form->method) @method($form->method) @endisset

        @foreach ($form->inputs as $input)
            @if ($input->type == 'hidden')
                <input
                    name="{{ $input->name }}"
                    type="{{ $input->type }}"
                    value="{{ $input->value }}"
                />
            @else
                @if ($errors->has($input->name))
                    <span class="invalid-feedback">{{ $errors->first($input->name) }}</span>
                @endif
                @isset ($input->label)
                    <label for="{{ $input->label }}">{{ $input->label }}</label>
                @endisset

                @switch ($input->type)
                    @case ('textarea')
                        <textarea
                            name="description"
                        >{{ old($input->name, optional($input)->value) }}</textarea>
                    @break
                    @default
                        <input
                            name="{{ $input->name }}"
                            type="{{ $input->type }}"
                            value="{{ old($input->name, optional($input)->value) }}"
                            @if (isset($input->required) ? $input->required : false)
                                required
                            @endif
                            @isset($input->min)
                                min="{{ $input->min }}"
                            @endisset
                            @isset($input->max)
                                max="{{ $input->max }}"
                            @endisset
                        />
                    @break
                @endswitch
            @endif
        @endforeach
    </form>
@endsection
