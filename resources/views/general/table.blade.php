@extends('layouts.app')

@section('content_class', 'table')

@section('content')
<h1>{{ $header }}</h1>

@include('includes.table')
@endsection
