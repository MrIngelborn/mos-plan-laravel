@extends('layouts.app')

@section('content_class', 'role')

@push('scripts')
    <script lang="javascript" type="text/javascript">
        function childChanged(container, checked) {
            var children = container.children('ul').children('li');
            var parent = container.parent().parent();
            var checkbox = container.children('input[type="checkbox"]');
            var checkbox_checked = checkbox.prop('checked');
            var checkbox_indeterminate = checkbox.prop('indeterminate');
            
            children.each(function() {
                return all_same = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
            });
            console.log('All: '+all_same);
            
            if (all_same) {
                checkbox.prop({
                    'checked': checked,
                    'indeterminate': false
                });
            }
            else {
                checkbox.prop({
                    'checked': false,
                    'indeterminate': true
                });
            }
            
            if (checkbox.prop('checked') != checkbox_checked || checkbox.prop('indeterminate') != checkbox_indeterminate) {
                if (parent.is('li')) {
        			childChanged(parent, checked);
    			}
            }
        }
        
        $(document).ready(function() {
			console.log("Document ready");
			
			$(".tree li > i").click(function() {
    			console.log("click");
    			var li = $(this).parent();
    			if (li.hasClass('expanded')) {
        			li.removeClass('expanded');
                }
                else {
                    li.addClass('expanded');
                }
			});
			
			$('.tree input[type="checkbox"]').change(function(event) {
    			console.log("check");
    			var checked = $(this).prop('checked');
    			var container = $(this).parent();
    			var siblings = container.siblings();
    			var parent = container.parent().parent();
    			
    			// Check / Uncheck all children
    			container.find('input[type="checkbox"]').prop({
        			indeterminate: false,
                    checked: checked
    			});
    			
    			// Notify parent of change
    			if (parent.is('li')) {
        			childChanged(parent, checked);
    			}
			});
			
			$('form').submit(function(event) {
    			var checked = $('.tree input[type="checkbox"]').filter(function() {
                    return this.checked === true;
                });
                checked.parent().children('ul').find('input[type="checkbox"]').prop('checked', false);
			});
			
			// Check the checkboxes
			$('input[checked]').change();
		});
    </script>
@endpush

@section('content')
<h1>Editera roll</h1>
<h2>{{ $role->name }}</h2>

<form method="post" action="{{ route('roles.update', $role->id) }}">
    @csrf
    @method('PUT')
    <label for="name">Name</label>
    <input name="name" type="text" value="{{ $role->name }}"/>
    <label for="slug">Slug</label>
    <input name="slug" type="text" value="{{ $role->slug }}"/>
    <h3>Permissions</h3>
    <ul class="tree"><li class="expanded">
        <i></i>
        <input type="checkbox" name="permissions[]" value="*"
        @isset ($permissions->checked) checked @endisset />
        <label for="*">*</label>
        <ul>
            @foreach ($permissions->children as $name_1 => $permissions)
                <li>
                    @isset ($permissions->children)
                        <i></i>
                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.*"
                        @isset ($permissions->checked) checked @endisset />
                        <label>{{ $name_1 }}</label>
                        <ul>
                            @foreach ($permissions->children as $name_2 => $permissions)
                                <li>
                                    @isset ($permissions->children)
                                        <i></i>
                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}.*"
                                        @isset ($permissions->checked) checked @endisset />
                                        <label>{{ $name_2 }}</label>
                                        <ul>
                                            @foreach ($permissions->children as $name_3 => $permissions)
                                                <li>
                                                    @isset ($permissions->children)
                                                        <i></i>
                                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}.{{ $name_3 }}.*"
                                                        @isset ($permissions->checked) checked @endisset
                                                        />
                                                        <label>{{ $name_3 }}</label>
                                                        <ul>
                                                            @foreach ($permissions->children as $name_4 => $permissions)
                                                                <li>
                                                                    @isset ($permissions->children)
                                                                        <i></i>
                                                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}.{{ $name_3 }}.{{ $name_4 }}.*"
                                                                        @isset ($permissions->checked) checked @endisset
                                                                        />
                                                                        <label>{{ $name_4 }}</label>
                                                                        <ul>
                                                                            
                                                                        </ul>
                                                                    @else
                                                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}.{{ $name_3 }}.{{ $name_4 }}.*"
                                                                        @isset ($permissions->checked) checked @endisset
                                                                        />
                                                                        <label>{{ $name_4 }}</label>
                                                                    @endisset
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    @else
                                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}.{{ $name_3 }}"
                                                        @isset ($permissions->checked) 
                                                            checked
                                                        @endisset
                                                        />
                                                        <label>{{ $name_3 }}</label>
                                                    @endisset
                                                </li>
                                            @endforeach
                                        </ul>
                                    @else
                                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}.{{ $name_2 }}"
                                        @isset ($permissions->checked) checked @endisset
                                        />
                                        <label>{{ $name_2 }}</label>
                                    @endisset
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <input type="checkbox" name="permissions[]" value="{{ $name_1 }}"
                        @isset ($permissions->checked) checked @endisset
                        />
                        <label>{{ $name_1 }}</label>
                    @endisset
                </li>
            @endforeach
        </ul>
    </li></ul>
    <input type="submit" value="Uppdatera" />
</form>

@endsection