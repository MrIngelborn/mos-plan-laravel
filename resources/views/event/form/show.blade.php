@php use Carbon\Carbon; @endphp

@extends('layouts.app')

@section('content_class', 'event_info')

@section('content')
    <h1>{{ __( 'event.form.for', ['name' => $event->name ] ) }}</h1>

    @if($form)
        <div id="info">
            <span class="label">{{ __('event.form.registration_opens_at') }}:</span>
            <span class="value">{{ Carbon::make($form->registration_opens_at) }}</span>

            <span class="label">{{ __('event.form.registration_closes_at') }}:</span>
            <span class="value">{{ Carbon::make($form->registration_closes_at) }}</span>
        </div>
        <div id="description">
            <h2>{{ __('event.form.inputs') }}</h2>
            @include('includes.table')
        </div>
    @else
        <div id="description">
            <p>{{ __( 'event.form.missing') }}</p>
        </div>
    @endif
@endsection


