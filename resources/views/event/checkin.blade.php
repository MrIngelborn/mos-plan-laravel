@extends('layouts.app')

@section('header', '')

@section('content_class', 'checkin')

@section('content')
    <script type="text/javascript" lang="javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $("#notice").hide(
                        "fade", 
                        2000, 
                        function() {
                    $(this).remove();
                });
            }, 2000);
        });
    </script>
    
    <h1>Checkin - {{ $event->name }}</h1>
    @if ($errors->any())
        <div id="notice">
            <h1 class="red">Error</h1>
            @foreach ($errors->all() as $error)
                <h2 class="red">{{ $error }}</h2>
            @endforeach
        </div>
    @else 
        @isset ($status)
            <div id="notice">
                <h1>{{ $user->name }}</h1>
                @if ($first_checkin)
                    <h2 class="green">Välkommen till {{ $event->name }}</h2>
                @else
                    @switch($status)
                        @case(0)
                            <h2 class="red">Hejdå, välkommen åter</h2>
                            @break
                        @case(1)
                            <h2 class="green">Välkommen tillbaka</h2>
                            @break
                        @default
                            <h2 class="red">Error: Konstig status</h2>
                    @endswitch
                @endif
                <h2 class="red">Aktuell skuld: {{ $debt }}</h2>
            </div>
        @endisset
    @endif
    
    <form action="{{ route('events.checkin.store', $event) }}" method="POST">
        @csrf
        
        <label for="barcode">Barcode</label>
        <input id="barcode" name="barcode" type="text" autofocus/>
        
        <input type="submit" value="Checkin" />
    </form>
@endsection