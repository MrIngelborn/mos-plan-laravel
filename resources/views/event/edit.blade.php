@extends('layouts.app')

@section('content')
    <h1>Edit Event</h1>
    
    <form action="{{ route('events.update', $event->uri) }}" method="post">
        @csrf
        @method('PUT')
        
        @if ($errors->has('uri'))
            <span class="error">
                {{ $errors->first('uri') }}
            </span>
        @endif
        <label for="uri">URI</label>
        <input id="uri" name="uri" type="text" value="{{ $event->uri }}"/>
        
        <label for="name">Name</label>
        <input id="name" name="name" type="text" value="{{ $event->name }}"/>
        
        <label for="start_time">Start Time</label>
        <input id="start_time" name="start_time" type="datetime" value="{{ $event->start_time }}"/>
        
        <label for="end_time">End Time</label>
        <input id="end_time" name="end_time" type="datetime" value="{{ $event->end_time }}"/>
        
        <label for="entry_price">Entry Price</label>
        <input id="entry_price" name="entry_price" type="number" value="{{ $event->entry_price }}"/>
        
        <label for="max_seats">Max Seats</label>
        <input id="max_seats" name="max_seats" type="number" value="{{ $event->max_seats }}"/>
        
        <label for="barcode_prefix">Barcode Prefix</label>
        <input id="barcode_prefix" name="barcode_prefix" type="text" value="{{ $event->barcode_prefix }}"/>
        
        <label for="max_debt">Max Debt</label>
        <input id="max_debt" name="max_debt" type="number" value="{{ $event->max_debt }}"/>
        
        <label for="description">Description</label>
        <textarea id="description" name="description"> {{ $event->description }} </textarea>
        
        <input type="submit" value="Update" />
    </form>
@endsection