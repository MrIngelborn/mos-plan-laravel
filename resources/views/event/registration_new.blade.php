@extends('layouts.app')

@section('content_class', 'registration')

@section('content')
<h1>Ny registrering för {{ $event_name }}</h1>
@if ($inputs == NULL)
    
    <h2>Registrering är inte tillgänglig</h2>
    
    @if($form_errors->any())
        @foreach($form_errors->all() as $error)
            <p class="error">{{ $error }}</p>
        @endforeach
    @endif
    
@else 
    
    @if($form_errors->any())
        @foreach($form_errors->all() as $error)
            <p class="error">{{ $error }}</p>
        @endforeach
    @endif
    
    <h2>Entrépris: {{ $event_price }}kr</h2>
    
    <form action="{{$form_action}}" method="POST">
        @csrf
        
        <label for="user">Användare</label>
        <select id="user" name="user">
            @foreach ($users as $user)
                <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach
        </select>
        
        @foreach ($inputs as $input)
            @if($input->type == 'textarea')
                <label for="{{ $input->name }}" style="justify-self: left">
                    {{ $input->label }}
                </label>
                <textarea 
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    {{ $input->disabled }}
                >{{ $input->value }}</textarea>
            @elseif ($input->type == 'checkbox')
                <label for="{{ $input->name }}">
                    {{ $input->label }}
                    @isset ($input->price)
                        ({{ $input->price }}kr)
                    @endisset
                </label>
                <input
                    type="checkbox"
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    @if ($input->value == 'on') checked @endif
                    {{ $input->disabled }}
                />
            @else
                <label for="{{ $input->name }}">
                    {{ $input->label }}
                    @isset ($input->price)
                        ({{ $input->price }}kr)
                    @endisset
                </label>
                <input
                    type="{{ $input->type }}"
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    value="{{ $input->value }}"
                    {{ $input->disabled }}
                />
            @endif
        @endforeach
        
        <input type="submit" value="Registrera"/>
    </form>
    
@endif
@endsection
