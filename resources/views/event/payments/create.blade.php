@extends('layouts.app')

@section('content')

<h1>Ny betalning</h1>
<form action="{{ route('events.payments.store', $event->uri) }}" method="POST">
    @csrf
    
    <label for="user">Användare</label>
    <select id="user" name="user">
        @foreach ($users as $user)
            <option value="{{ $user->id }}">{{ $user->name }}</option>
        @endforeach
    </select>
    
    <label for="amount">Summa</label>
    <input id="amount" name="amount" type="number" />
    
    <input type="submit" value="Registrera Betalning">
</form>

@endsection
