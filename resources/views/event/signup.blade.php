@extends('layouts.app')

@section('content_class', 'registration')

@section('content')
<h1>Anmälan för {{ $event_name }}</h1>
@if ($inputs == NULL)
    
    <h2>Registrering är inte tillgänglig</h2>
    
    @if($form_errors->any())
        @foreach($form_errors->all() as $error)
            <p class="error">{{ $error }}</p>
        @endforeach
    @endif
    
@else 
    
    @if($form_errors->any())
        @foreach($form_errors->all() as $error)
            <p class="error">{{ $error }}</p>
        @endforeach
    @endif
    
    <h2>Entrépris: {{ $event_price }}kr</h2>
    
    <form action="" method="POST">
        @csrf
        
        @isset ($editing) @method('PUT') @endisset
        
        @foreach ($inputs as $input)
            @if($input->type == 'textarea')
                <label for="{{ $input->name }}" style="justify-self: left">
                    {{ $input->label }}
                </label>
                <textarea 
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    {{ $input->disabled }}
                >{{ $input->value }}</textarea>
            @elseif ($input->type == 'checkbox')
                <label for="{{ $input->name }}">
                    {{ $input->label }}
                    @isset ($input->price)
                        ({{ $input->price }}kr)
                    @endisset
                </label>
                <input
                    type="checkbox"
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    @if ($input->value == 'on') checked @endif
                    {{ $input->disabled }}
                />
            @else
                <label for="{{ $input->name }}">
                    {{ $input->label }}
                    @isset ($input->price)
                        ({{ $input->price }}kr)
                    @endisset
                </label>
                <input
                    type="{{ $input->type }}"
                    name="{{ $input->name }}"
                    placeholder="{{ $input->placeholder }}"
                    value="{{ $input->value }}"
                    {{ $input->disabled }}
                />
            @endif
        @endforeach
        <label for="policy">Jag godkänner att mina uppgifter hanteras av MoS</label>
        <input id="policy" name="policy" type="checkbox" required />
        
        @isset ($editing)
            <input type="submit" value="Uppdatera"/>
        @else
            <input type="submit" value="Registrera"/>
        @endisset
    </form>
    
@endif
@endsection
