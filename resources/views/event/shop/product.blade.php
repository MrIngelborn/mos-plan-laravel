@extends('layouts.app')

@section('content_class', 'table')

@section('content')
<h1>{{ $header }}</h1>

<form method="POST" class="double-action">
    @csrf
    @method('PUT')
    
    @if ($errors->has('name'))
        <span class="invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
    <label for="name">Name</label>
    <input id="name" name="name" type="text" value="{{ old('name', $product->name) }}" required />
    
    @if ($errors->has('price'))
        <span class="invalid-feedback">{{ $errors->first('price') }}</span>
    @endif
    <label for="price">Price</label>
    <input id="price" name="price" type="number" value="{{ old('price', $product->price) }}" required />
    
    @if ($errors->has('barcode'))
        <span class="invalid-feedback">{{ $errors->first('barcode') }}</span>
    @endif
    <label for="barcode">Barcode</label>
    <input id="barcode" name="barcode" type="text" value="{{ old('barcode', $product->barcode) }}" required />
    
    <a href="" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Are you sure?"><button>DELETE</button></a>
    <input type="submit" value="Update" />
</form>

@endsection
