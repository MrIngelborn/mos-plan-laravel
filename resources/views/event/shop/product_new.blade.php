@extends('layouts.app')

@section('content_class', 'table')

@section('content')
<h1>Ny produkt</h1>

<form action="{{ $action }}" method="POST">
    @csrf
    
    @if ($errors->has('name'))
        <span class="invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
    <label for="name">Name</label>
    <input id="name" name="name" type="text" value="{{ old('name') }}" required />
    
    @if ($errors->has('price'))
        <span class="invalid-feedback">{{ $errors->first('price') }}</span>
    @endif
    <label for="price">Price</label>
    <input id="price" name="price" type="number" value="{{ old('price') }}" required />
    
    @if ($errors->has('barcode'))
        <span class="invalid-feedback">{{ $errors->first('barcode') }}</span>
    @endif
    <label for="barcode">Barcode</label>
    <input id="barcode" name="barcode" type="text" value="{{ old('barcode') }}" required />
    
    <input type="submit" value="Skapa" />
</form>

@endsection
