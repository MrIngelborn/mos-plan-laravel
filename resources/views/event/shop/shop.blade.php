@extends('layouts.app')

@section('header', '')

@section('content_class', 'shop')

@section('content')
<h1>Shop</h1>

@isset ($notice)
    <script type="text/javascript" lang="javascript">
        $(document).ready(function() {
            setTimeout(function() {
                $("#notice").hide(
                        "fade", 
                        2000, 
                        function() {
                    $(this).remove();
                });
            }, 2000);
        });
    </script>
    <div id="notice">
        <h1 class="{{ $notice->color }}">{{ $notice->message }}</h1>
    </div>
@endisset


<form method="post" class="double-action">
    @csrf
    @if ($errors->any())
        <span class="invalid-feedback">{{ $errors->first() }}</span>
    @endif
    <input name="barcode" type="text" autofocus />
    <input type="submit" value="Enter"/>
</form>
@isset($message)
    <span class="message">
        {{ $message }}
    </span>
@endif

@isset($cart) 
    <h2>Varukorg</h2>
    <table id="cart">
        <thead>
            <tr>
                <th>Namn</th>
                <th>Mängd</th>
                <th>Pris</th>
                <th>Totalt pris</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($cart as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->amount }}</td>
                    <td>{{ $product->price }} kr</td>
                    <td>{{ $product->total_price }} kr</td>
                </tr>
            @endforeach
            <tr>
                <td>SUM</td>
                <td/><td/>
                <td>{{ $sum }} kr</td>
            </tr>
        </tbody>
    </table>
@endisset
    
@empty($cart)
    <h2>Ny kund</h2>   
    <p>Scanna en produkt för att lägga den i varukorgen.</p>
@else
    <h2>Klar?</h2>
    <p>Skanna ditt ID-kort för att avsluta köpet.</p>
@endempty
@endsection
