@extends('layouts.app')

@section('content_class', 'barcodes')

@section('header', '')

@push('scripts')
    <script src="{{ asset('js/JsBarcode.all.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            JsBarcode(".barcode").init();
        });
    </script>
@endpush

@section('content')
    <h1>Funktionter</h1>
    <div class="barcodes">
    @foreach ($barcodes as $barcode)
        <svg
            class="barcode"
            jsbarcode-value="{{ $barcode->barcode }}" 
            jsbarcode-text="{{ $barcode->title }}"
        ></svg>
    @endforeach
    </div>
    
    <h1>Produkter</h1>
    <div class="products">
    @foreach ($products as $product)
        <svg 
            class="barcode" 
            jsbarcode-value="{{ $product->barcode }}" 
            jsbarcode-text="{{ $product->name }} | {{ $product->price }}:-"
        ></svg>
    @endforeach
    </div>
@endsection