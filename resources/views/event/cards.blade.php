@extends('layouts.app')

@section('content_class', 'cards')

@section('header', '')

@push('scripts')
    <script src="{{ asset('js/jquery.textfill.min.js') }}" lang="javascript" type="text/javascript"></script>@push('scripts')
    <script src="{{ asset('js/JsBarcode.all.min.js') }}"></script>
@endpush
    <script lang="javascript" type="text/javascript">
        $(document).ready(function() {
            // Generate barcodes
            JsBarcode("svg.barcode").init();
            
            // Make sure the names fit
			$(".card .name").textfill({
				debug: true,
				widthOnly: true
			});
		});
    </script>
@endpush

@section('content')
    @foreach ($cards as $card)
        <div class="card">
            <div class="name">
                <span>{{ $card->username }}</span>
            </div>
            <div class="barcode">
                <svg 
                    class="barcode"
                    jsbarcode-height="160"
                    jsbarcode-value="{{ $card->barcode }}"
                ></svg>
            </div>
            <div><div class="boxes">
                @foreach ($card->boxes as $box)
				    <span class="box{{ $box->checked ? ' checked' : '' }}"></span>
				    <span>{{ $box->label }}</span>
                @endforeach
			</div></div>
        </div>
    @endforeach
@endsection