@extends('layouts.app')

@section('content_class', 'event_info')

@section('content')
    <h1>{{ $event->name }}</h1>
    <div id="info">
        @switch ($event->registrationAvailablity())
            @case (-1)
                <span class="label error">Registrering är tillgänglig från: </span>
                <span class="value error">{{ $event->registrationForm->registration_opens_at }}</span>
                @break
            @case (0)
                <span class="label">Registreringen är öppen:</span>
                <a class="value" href="{{ route('events.signup', $event) }}">
                    Registrera dig nu!
                </a>
                @break
            @case (1)
                <span class="label error">Registreringen stängde:</span>
                <span class="value error">{{ $event->registrationForm->registration_closes_at }}</span>
                @break
        @endswitch
        <span class="label">Entrépris:</span>
        <span class="value">{{ $event->entry_price }}kr</span>
        <span class="label">Start:</span>
        <span class="value">{{ \Carbon\Carbon::parse($event->start_time)->format('d M Y, H:i') }}</span>
        <span class="label">Slut:</span>
        <span class="value">{{ \Carbon\Carbon::parse($event->end_time)->format('d M Y, H:i') }}</span>
    </div>
    <div id="description">
        {!! $event->description !!}
    </div>
@endsection