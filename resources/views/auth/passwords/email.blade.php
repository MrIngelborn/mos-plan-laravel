@extends('layouts.app')

@section('content')
<h1>{{ __('Reset Password') }}</h1>

@if (session('status'))
    <p class="alert alert-success">
        {{ session('status') }}
    </p>
@endif

<form method="POST" action="{{ route('password.email') }}">
    @csrf

    <label for="email">{{ __('E-Mail Address') }}</label>

    <input id="email" 
            type="email" 
            class="{{ $errors->has('email') ? ' is-invalid' : '' }}" 
            name="email" 
            value="{{ old('email') }}" 
            required
    />

    @if ($errors->has('email'))
        <span class="invalid-feedback">
            {{ $errors->first('email') }}
        </span>
    @endif
    <button type="submit">
        {{ __('Send Password Reset Link') }}
    </button>
</form>
</div>
@endsection
