@extends('layouts.app')

@section('content')
<h1>{{ __('Login') }}</h1>
<form method="POST" action="{{ route('login') }}" class="double-action">
    @csrf

    @if ($errors->has('email'))
        <span class="invalid-feedback">
            {{ $errors->first('email') }}
        </span>
    @endif

    <label for="email">E-postadress</label>
    <input id="email"
            type="email"
            class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
            name="email"
            value="{{ old('email') }}"
            required
            autofocus
    />

    <label for="password">Lösenord</label>

    <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required />

    <label for="remember">
        Håll mig inloggad
    </label>

    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />

    <button type="submit" class="btn btn-primary">
        Logga in
    </button>

    @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}">
            <button type="button">Glömt ditt lösenord?</button>
        </a>
    @endif
        </div>
    </div>
</form>
@endsection
