@extends('layouts.app')

@section('content_class', 'register')

@section('content')
    <h1>Bli medlem i Mys och Spel</h1>

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <label for="name" class="required">Namn</label>
        <input id="name"
               type="text"
               class="{{ $errors->has('name') ? ' is-invalid' : '' }}"
               name="name"
               value="{{ old('name') }}"
               placeholder="Förnamn Efternamn"
               required
               autofocus/>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
            {{ $errors->first('name') }}
        </span>
        @endif

        <label for="ssn" class="required">Personnummer</label>
        <input id="ssn"
               type="text"
               name="ssn"
               value="{{ old('ssn') }}"
               class="{{ $errors->has('ssn') ? ' is-invalid' : '' }}"
               placeholder="yyyymmdd-nnnn"
               pattern="[0-9]{6,8}-{0,1}[0-9]{4}"
               title="Måste vara ett giltigt personnummer"
               required/>
        @if ($errors->has('ssn'))
            <span class="invalid-feedback">
            {{ $errors->first('ssn') }}
        </span>
        @endif

        <label for="phone1" class="required">Telefonnummer</label>
        <input id="phone1"
               type="text"
               name="phone1"
               value="{{ old('phone1') }}"
               class="{{ $errors->has('phone1') ? ' is-invalid' : '' }}"
               placeholder="+46701234567"
               pattern="+{0,1}\d+"
               required/>
        @if ($errors->has('phone1'))
            <span class="invalid-feedback">
            {{ $errors->first('phone1') }}
        </span>
        @endif

        <label for="email" class="required">E-postaddress</label>
        <input id="email"
               type="email"
               class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
               name="email"
               value="{{ old('email') }}"
               placeholder="namn@domän.se"
               required
        />
        @if ($errors->has('email'))
            <span class="invalid-feedback">
            {{ $errors->first('email') }}
        </span>
        @endif

        <label for="password" class="required">Lösenord</label>
        <input id="password"
               type="password"
               class="{{ $errors->has('password') ? ' is-invalid' : '' }}"
               name="password"
               required
        />

        @if ($errors->has('password'))
            <span class="invalid-feedback">
            {{ $errors->first('password') }}
        </span>
        @endif
        <label for="password-confirm" class="required">Konfirmera lösenord</label>
        <input id="password-confirm" type="password" name="password_confirmation" required/>

        <input id="valid" type="checkbox" name="valid" required/>
        <label class="after required" for="valid">Jag försäkrar att de uppgifter jag lämnat är sanningsenliga</label>

        <div>
            <h2>Hantering av dina uppgifter</h2>
            <p>
                Du ansöker nu om medlemsskap i föreningen under {{ now()->year }}. Du ansluts också till Sverok. Är du
                mellan 6-25 år, så får föreningen bidrag för att du är medlem. Inga personuppgifter kommer att lämnas ut
                av Sverok.
            </p>
            <p>
                Mys och Spel använder Mailchimp för utskick av information till sina medlemmar.
            </p>
            <h3>Så behandlas dina personuppgifter</h3>
            <p>
                Den förening du nu blir medlem i ansvarar för att dina personuppgifter behandlas med noggrann
                försiktighet och att dessa inte lämnas vidare till annan part, utan ditt samtycke.
            </p>
            <p>
                Du lämnar nu dina personuppgifter i databasen eBas, som är ett rapportsystem för alla medlemsföreningar
                i
                riksförbundet Sverok. När du blir medlem i den här föreningen blir du även medlem i Sverok.
            </p>
            <p>
                Sverok följer Dataskyddsförordningen (GDPR) och behandlar dina uppgifter med noggrann försiktighet. Dina
                personuppgifter lämnas aldrig ut till andra av Sverok. Sverok får spara dina personuppgifter i upp till
                tre år.
            </p>
            <p>
                Sverok använder dina personuppgifter vid ringkontroller, för att säkerställa föreningens medlemslista.
                Då ringer vi till slumpvis utvalda medlemmar och frågar om de är medlemmar och om de varit med och
                spelat eller varit med på ett annat föreningsarrangemang.
            </p>
            <p>
                Sverok skickar kontinuerligt utskick till alla medlemmar i förbundet. Det kan även förekomma
                enkätundersökningar som du ombeds svara på.
            </p>
            <p>
                Uppgifterna används även som underlag vid statsbidragsansökan, distriktens landstingsbidragsansökningar
                och statistik.
            </p>
            <p>
                Granskare från statlig myndighet eller region/landstingsförbund kan få möjlighet att granska
                personuppgifter från Sveroks förbundskansli, om deras arbete kräver det.
            </p>
        </div>

        <input id="policy" type="checkbox" name="policy" required/>
        <label class="after required" for="policy">Jag godkänner föreningens hantering av mina personuppgifter</label>

        <div>
            <h2>Mys och Spels stadgar</h2>
            <p>
                §1 Föreningens namn<br/>
                Föreningens namn är Mys och Spel<br/>
                Förkortning på föreningens namn är MoS
            </p>
            <p>
                §2 Föreningens säte<br/>
                Styrelsen har sitt säte i Ekerö kommun.
            </p>
            <p>
                §3 Föreningsform<br/>
                Föreningen är en ideell förening. Föreningen är ansluten till Sveriges roll och
                konfliktspelsförbund.
            </p>
            <p>
                §4 Syfte<br/>
                Föreningens syfte är främst att främja spelande av olika former av sällskaps- och
                datorspel.
            </p>
            <p>
                §5 Oberoende<br/>
                Föreningen är religiöst och partipolitiskt obunden.
            </p>
            <p>
                §6 Verksamhetsår<br/>
                Verksamhetsåret är 1 januari till 31 december.
            </p>
            <p>
                §7 Medlemmar<br/>
                Som medlem antas intresserad som godkänner dessa stadgar och aktivt tar ställning för
                ett medlemskap genom att årligen betala föreningens medlemsavgift eller, om
                medlemskapet är gratis, årligen göra en skriftlig anmälan till föreningen. Avgiftens storlek
                beslutas på årsmötet. Årsmötet kan besluta att det är gratis att vara medlem. En medlem
                som allvarligt skadar föreningen kan avstängas av styrelsen. Avstängd medlem måste
                diskuteras på nästa årsmöte. Antingen så upphävs då avstängningen eller så utesluts
                medlemmen. Styrelsen och årsmöte kan upphäva avstängning och uteslutning.
            </p>
            <p>
                §8 Styrelsen<br/>
                Styrelsen ansvarar för föreningens medlemslista, bidragsansökningar,
                medlemsvärvning, beslut som tas på årsmöten och övrig verksamhet. Föreningens
                styrelse består av ordförande, kassör och sekreterare. Vid behov kan även vice
                ordförande, extra ledamöter och suppleanter väljas. Samma person får inte ha flera
                poster i styrelsen. Styrelsen väljs på årsmöte och tillträder direkt efter valet. Valbar är
                medlem i föreningen.
            </p>
            <p>
                §9 Revisorer<br/>
                För granskning av föreningens räkenskaper och förvaltning väljs på årsmöte en eller två
                revisorer. Valbar är person som inte sitter i styrelsen. Revisor behöver inte vara medlem
                i föreningen
            </p>
            <p>
                §10 Valberedning<br/>
                För att ta fram förslag på personer till de i stadgarna föreskrivna valen kan årsmötet välja
                en eller flera valberedare. Valbar är medlem i föreningen.
            </p>
            <p>
                §11 Ordinarie årsmöte<br/>
                Ordinarie årsmöte ska hållas senast den 31 mars varje år. Styrelsen beslutar om tid och
                plats. För att vara behörigt måste föreningens medlemmar meddelas minst två veckor i
                förväg. Följande ärenden ska alltid behandlas på ordinarie årsmöte:
                1. ) mötets öppnande
                2. ) mötets behörighet
                3. ) val av mötets ordförande
                4. ) val av mötets sekreterare
                5. ) val av två personer att justera protokollet
                6. ) styrelsens verksamhetsberättelse för förra året
                7. ) ekonomisk berättelse för förra året
                8. ) revisorernas berättelse för förra året
                9. ) ansvarsfrihet för förra årets styrelse
                10. ) årets verksamhetsplan
                11. ) årets budget och fastställande av medlemsavgift
                12. ) val av årets styrelse
                13. ) val av årets revisor
                14. ) val av årets valberedare
                15. ) övriga frågor
                16. ) mötets avslutande
            </p>
            <p>
                §12 Extra årsmöte<br/>
                Om styrelsen vill, eller revisor eller minst hälften av föreningens medlemmar kräver det
                ska styrelsen kalla till extra årsmöte. Vid giltigt krav på extra årsmöte kan den som krävt
                det sköta kallelsen. För att vara behörigt måste föreningens medlemmar meddelas minst
                två veckor i förväg. På extra årsmöte kan bara de ärenden som nämnts i kallelsen
                behandlas.
            </p>
            <p>
                §13 Firmateckning<br/>
                Föreningens firma tecknas av ordförande och kassör var för sig. Om särskilda skäl
                föreligger kan annan person utses att teckna föreningens firma
            </p>
            <p>
                §14 Rösträtt<br/>
                Endast närvarande medlem enligt §7 har rösträtt på årsmöte. På styrelsemöten har
                närvarande ordinarie ledamot rösträtt. Frånvarande ledamots röst ges till suppleant, i
                ordning bestämd av årsmötet
            </p>
            <p>
                §15 Röstetal<br/>
                Alla frågor som behandlas på årsmöte eller styrelsemöte avgörs med enkel röstövervikt
                om inget annat står i stadgarna. Nedlagda röster räknas ej. Varje person med rösträtt
                har en röst. Vid lika röstetal får slumpen avgöra.
                §15.1 Röstetal på styrelsemöte
                Alla frågor som behandlas på styrelsemöte avgörs med absolut majoritet.
                Nedlagda röster räknas ej.
            </p>
            <p>
                §16 Stadgeändring<br/>
                Dessa stadgar kan bara ändras på årsmöte. För att vara giltig måste ändringen antas
                med två tredjedelar av antalet röster. Då stadgeändring ska ske måste förslaget delges
                medlemmarna i kallelsen till mötet. I annat fall måste ändringen antas enhälligt. Ändring
                av föreningens stadgar om föreningsform (§3), syfte (§4), stadgeändringar (§16) och
                upplösning (§17) kräver likalydande beslut på två på varandra följande ordinarie
                årsmöten.
            </p>
            <p>
                §17 Upplösning<br/>
                Förslag om föreningens upplösning får endast framläggas på årsmöte. Att upplösning
                ska behandlas måste framgå av kallelsen. Föreningen kan inte upplösas så länge minst
                tre(3) medlemmar vägrar godkänna upplösningen. Vid upplösning ska föreningens
                skulder betalas. Därefter ska föreningens tillgångar gå till verksamhet i enlighet med
                föreningens syfte. Hur detta ska ske beslutas på det sista årsmötet.
            </p>
        </div>

        <input id="bylaws" type="checkbox" name="bylaws" required/>
        <label class="after required" for="bylaws">Jag har läst och godkänner Mys och Spel's stadgar</label>

        <button type="submit">
            Bli medlem
        </button>
    </form>
@endsection
