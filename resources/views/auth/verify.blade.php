@extends('layouts.app')

@section('content')
    <h1>Verifiera din epost-adress</h1>
    @if (session('resent'))
        <p >
            Ett nytt mail har skickats till din epostadress.
        </p>
    @endif
    <p>
        Innan du fortsätter behöver du verifiera din epost-adress. Du ska ha fått ett mail till den adressen du angav.
        Om du inte fick något mail
        <a href="{{ route('verification.resend') }}">klicka här för att begära ett till</a>.
    </p>
@endsection
