<table class="model_list">
    <thead>
        <tr>
            @foreach ($table?->headers as $header)
                <th>{{ $header }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($table?->rows as $row)
            <tr>
                @foreach ($row as $cell)
                    <td>
                        @isset ($cell->link)
                            @isset ($cell->delete)
                                <a href="{{ $cell->link }}"
                                        data-method="delete"
                                        data-token="{{csrf_token()}}"
                                        data-confirm="Are you sure?">
                                    <button>{{ $cell->value }}</button>
                                </a>
                            @else
                                <a href="{{ $cell->link }}">{{ $cell->value }}</a>
                            @endisset
                        @elseif (is_bool($cell))
                            <input type="checkbox" disabled
                                    @if ($cell) checked @endif
                            />
                        @else
                            {{ $cell }}
                        @endisset
                    </td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
