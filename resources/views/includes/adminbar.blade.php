@isset ($adminbar)
<!-- Admin bar -->
<nav id="adminbar">
    <h2>Admin:</h2>
    @foreach ($adminbar as $text => $href)
        <a href="{{ $href }}">{{ $text }}</a>
    @endforeach
</nav>
@endisset