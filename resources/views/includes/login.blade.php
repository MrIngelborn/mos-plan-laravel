<!-- Authentication Form -->
<hgroup id="login">
    @guest
        <a href="{{ route('login') }}">Logga in</a>

        <a href="{{ route('register') }}">Bli medlem</a>

    @else
	    <a href="{{ route('home.show') }}">{{ Auth::user()->name }}</a>
	    <a href="{{ route('logout') }}" onclick="
    	        event.preventDefault();
                document.getElementById('logout_form').submit();">
            Logga ut
        </a>
        <form id="logout_form" action="{{ route('logout') }}" method="POST">
            @csrf
        </form>
    @endguest
</hgroup>
