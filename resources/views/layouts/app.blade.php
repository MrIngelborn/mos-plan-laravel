<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Application') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/laravel.js') }}"></script>
    @stack('scripts')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @section('header')
        <header>
            <hgroup class="title">
                <a href="
                        @if (Route::has('index'))
                            {{ route('index') }}
                        @else
                            {{ url('/') }}
                        @endif
                        ">{{ config('app.name', 'Laravel') }}</a>
            </hgroup>
            @section('login')
                @include('includes.login')
            @show
        </header>
    @show
    @include('includes.adminbar')
    <section id="app" class="@yield('content_class')">
        @yield('content')
    </section>
</body>
</html>
