@extends('layouts.app')

@section('content')
    <h1>{{ config('app.name', 'Application') }}</h1>
    <section>
        <p>
            Välkommen till MoS PLAN-portal! <br/>
            Skapa ett konto uppe till höger för att kunna registrera dig på våra arrangemang.
        </p>
        <p>
            Kontakta oss på
            <a href="mailto:info@mysochspel.se" target="_blank">info@mysochspel.se</a>
            om det är något du undrar över.
        </p>
        <h2>Arrangemangsinformation PLAN 2019</h2>
        <iframe src="https://docs.google.com/document/d/e/2PACX-1vQ0dSjzHFN4ODoWgC-0mcW0-Rcvr7Z4pCprj5bpjPoQvKdRE8XKHrbrMBzo6SVb42wimTyPF5S7uVUc/pub?embedded=true" style="width: 100%; border: 1px black solid; height: 40em;"></iframe> 
    </section>
@endsection