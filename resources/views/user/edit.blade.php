@extends('layouts.app')

@section('content_class', 'form')

@section('content')
<h1>Redigera användare</h1>

<form action="{{ route('users.update', $user->id) }}" method="POST">
    @csrf
    @method('PUT')

    <label for="name">Namn</label>
    <input id="name" name="name" type="text" value="{{ $user->name }}" />

    <label for="email">Epost</label>
    <input id="email" name="email" type="email" value="{{ $user->email }}" />

    @isset ($roles)
    <fieldset>
        <legend>Roles:</legend>

        @foreach ($roles as $role)
            <input name="roles[{{ $role->id }}]" value="{{ $role->id }}" type="checkbox" {{ $user->hasRole($role->slug) ? 'checked' : '' }}>
            <label for="{{ $role->slug }}">{{ $role->name }}</label>
            <br/>
        @endforeach
    </fieldset>
    @endisset

    <input type="submit"  value="Uppdatera"/>

</form>

@endsection
