@extends('layouts.app')

@section('content_class', 'event_info')

@section('content')
    <h1>{{ $user->name }}</h1>
    <div id="info">
        <span class="label">E-post:</span>
        <span class="value">{{ $user->email }}</span>
        <span class="label">Personnummer:</span>
        <span class="value">{{ $user->ssn }}</span>
        <span class="label">Telefonnummer:</span>
        <span class="value">{{ $user->phone1 }}</span>
        <span class="label">Kontot skapades:</span>
        <span class="value">{{ \Carbon\Carbon::parse($user->created_at)->format('d M Y, H:i') }}</span>
        <span class="label">Kontot uppdaterades:</span>
        <span class="value">{{ \Carbon\Carbon::parse($user->updated_at)->format('d M Y, H:i') }}</span>
        <span class="label">Uppdaterade medlemskap:</span>
        <span class="value">{{ \Carbon\Carbon::parse($user->last_renewed)->format('d M Y') }}</span>
        <a class="button" href="{{ route('users.edit', $user->id) }}">Redigera</a>
        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Radera" class="button"  onclick="return confirm('Are you sure?')" style="color: red"/>
        </form>
    </div>
@endsection
