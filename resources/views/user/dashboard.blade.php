@extends('layouts.app')

@section('content_class', 'event_info')

@section('content')
    <h1>Din profil</h1>

    <div id="info">
        <span class="label">Namn:</span>
        <span class="value">{{ $user->name }}</span>
        <span class="label">E-post:</span>
        <span class="value">{{ $user->email }}</span>
        <span class="label">Uppdaterade medlemskap:</span>
        <span class="value">{{ \Carbon\Carbon::parse($user->last_renewed)->format('d M Y') }}</span>
        <a class="button" href="{{ route('home.edit') }}">Redigera</a>
        <form action="{{ route('users.destroy', $user->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" value="Radera" class="button"  onclick="return confirm('Are you sure?')" style="color: red"/>
        </form>
    </div>

    @isset($event)
        <h2>Aktuellt arrangemang: <a href="{{ route('events.show', $event->uri) }}">{{ $event->name }}</a></h2>

        @isset($user_economy)
            <h3>Din ekonomi</h3>
            <div class="label_value">
                <span class="label">Entrékostnad:</span>
                <span class="value">{{ $user_economy->entry_price }}kr</span>
                <span class="label">Tilläggskostnader:</span>
                <span class="value">{{ $user_economy->event_extra }}kr</span>
                <span class="label"><a href="{{ route('home.purchases') }}">Spenderat i butik</a>:</span>
                <span class="value">{{ $user_economy->event_purchases }}kr</span>
                <hr />
                <span class="label">Betalat: </span>
                <span class="value">{{ $user_economy->payed }}kr</span>
                <span class="label">Aktuell skuld: </span>
                <span class="value">{{ $user_economy->debt }}kr</span>
            </div>
        @else
            <p>Du har inte registrerat dig ännu</p>
        @endisset
    @else
        <h2>Inget aktuellt arrangemang</h2>
    @endisset

@endsection
