<?php
use App\Http\Controllers\Event\CardsController;
use App\Http\Controllers\Event\CheckinController;
use App\Http\Controllers\Event\EconomyController;
use App\Http\Controllers\Event\EventController;
use App\Http\Controllers\Event\FormController;
use App\Http\Controllers\Event\FormInputController;
use App\Http\Controllers\Event\PaymentController;
use App\Http\Controllers\Event\ResponseController;
use App\Http\Controllers\Event\Shop\BarcodeController;
use App\Http\Controllers\Event\Shop\ProductController;
use App\Http\Controllers\Event\Shop\ShopController;
use App\Http\Controllers\Event\SignupController;
use App\Http\Controllers\Event\UserEconomyController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController')->name('index');

Route::singleton('profile', ProfileController::class)->names('home');

Route::resource('events', EventController::class);
Route::prefix('events/{event}')->name('events.')->group(function () {
    Route::resource('checkin', CheckinController::class)->only(['index', 'create', 'store']);
    Route::resource('payments', PaymentController::class)->only(['index', 'create', 'store', 'destroy']);
    Route::singleton('signup', SignupController::class)->creatable()->except(['destroy', 'edit']);
    Route::resource('responses', ResponseController::class);

    Route::singleton('form', FormController::class)->creatable();
    Route::prefix('form')->name('form.')->group(function () {
        Route::resource('input', FormInputController::class)->except(['index']);
    });

    Route::get('cards', CardsController::class)->name('cards');
    Route::get('economy', EconomyController::class)->name('economy');
    Route::get('user_economy', UserEconomyController::class)->name('user_economy');

    // Event shop
    Route::controller(ShopController::class)->prefix('shop')->name('shop.')->group(function () {
        Route::get('', 'show')->name('show');
        Route::post('', 'enterBarcode');

        // Products
        Route::resource('products', ProductController::class);

        // Barcode list
        Route::get('barcodes', BarcodeController::class)->name('barcodes');
    });
});

Route::resource('users', UserController::class);
Route::resource('roles', RoleController::class);

Auth::routes(['verify' => true]);
