<?php

namespace Tests\Feature;

use App\Models\Event;
use App\Models\Event\Registration\FormInput;
use App\Models\Role;
use App\Models\User;
use Database\Factories\Event\Registration\FormInputFactory;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class FormInputControllerTest extends TestCase
{
    private Event $event;
    private User $user;


    protected function setUp(): void
    {
        parent::setup();

        $this->event = Event::factory()->create();
        $this->event->form()->create();

        $this->user = User::factory()->create();
        Auth::login($this->user);
    }

    protected function tearDown(): void
    {
        $this->event->delete();
        $this->user->delete();

        parent::tearDown();
    }

    private function useAdminUser(): void
    {
        $adminRole = Role::where('slug', 'admin')->first();
        $this->user->roles()->save($adminRole);
    }

    public function test_adminCanShowCreatePage(): void
    {
        $this->useAdminUser();

        $response = $this->get(route('events.form.input.create', $this->event));
        $response->assertOk();
    }

    public function test_userCannotShowCreatePage(): void
    {
        $response = $this->get(route('events.form.input.create', $this->event));
        $response->assertForbidden();
    }

    public function test_adminCanCreateInput(): void
    {
        $this->useAdminUser();
        $formInputFactory = new FormInputFactory();
        $this->assertEquals(0, $this->event->form->inputs()->count());

        $this->post(route('events.form.input.store', $this->event), $formInputFactory->definition());
        $this->assertEquals(1, $this->event->form->inputs()->count());

        $this->post(route('events.form.input.store', $this->event), $formInputFactory->definition());
        $this->assertEquals(2, $this->event->form->inputs()->count());
    }

    public function test_userCannotCreateInput(): void
    {
        $formInputFactory = new FormInputFactory();
        $this->assertEquals(0, $this->event->form->inputs()->count());

        $response = $this->post(route('events.form.input.store', $this->event), $formInputFactory->definition());
        $response->assertForbidden();

        $this->assertEquals(0, $this->event->form->inputs()->count());
    }

    public function test_canShowViewPage(): void
    {
        $input = FormInput::factory()
            ->for($this->event->form)
            ->create();

        $response = $this->get(route('events.form.input.show', [$this->event, $input]));
        $response->assertOk();
    }
}
