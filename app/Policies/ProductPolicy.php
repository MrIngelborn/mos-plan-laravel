<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Product;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list all products.
     */
    public function list(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.products.list");
    }
    
    /**
     * Determine whether the user can view the product.
     */
    public function view(User $user, Product $product)
    {
        return $user->hasAccess("event.{$product->event->uri}.products.view");
    }

    /**
     * Determine whether the user can create products.
     */
    public function create(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.products.create");
    }

    /**
     * Determine whether the user can update the product.
     */
    public function update(User $user, Product $product)
    {
        return $user->hasAccess("event.{$product->event->uri}.products.update");
    }

    /**
     * Determine whether the user can delete the product.
     */
    public function delete(User $user, Product $product)
    {
        return $user->hasAccess("event.{$product->event->uri}.products.delete");
    }
}
