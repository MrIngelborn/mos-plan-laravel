<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Purchase;
use Illuminate\Auth\Access\HandlesAuthorization;

class PurchasePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can list the purchases for the event.
     */
    public function list(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.purchase.list");
    }
    
    /**
     * Determine whether the user can view the purchase.
     */
    public function view(User $user, Purchase $purchase)
    {
        return $user->hasAccess("event.{$purchase->event->uri}.purchase.list") or $user->id === $purchase->user_id;
    }

    /**
     * Determine whether the user can create purchases.
     */
    public function create(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.purchase.list");
    }

    /**
     * Determine whether the user can update the purchase.
     */
    public function update(User $user, Purchase $purchase)
    {
        return $user->hasAccess("event.{$$purchase->event->uri}.purchase.list");
    }

    /**
     * Determine whether the user can delete the purchase.
     */
    public function delete(User $user, Purchase $purchase)
    {
        return $user->hasAccess("event.{$purchase->event->uri}.purchase.list");
    }
}
