<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Event;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the event.
     */
    public function list(User $user)
    {
        return $user->hasAccess('event.list');
    }
    
    /**
     * Determine whether the user can view the event.
     */
    public function view(?User $user, Event $event)
    {
        return true;
    }

    /**
     * Determine whether the user can create events.
     */
    public function create(User $user)
    {
        return $user->hasAccess('event.create');
    }

    /**
     * Determine whether the user can update the event.
     */
    public function update(User $user, Event $event)
    {
        //return true;
        return $user->hasAccess("event.{$event->uri}.update");
    }

    /**
     * Determine whether the user can delete the event.
     */
    public function delete(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.delete");
    }
}
