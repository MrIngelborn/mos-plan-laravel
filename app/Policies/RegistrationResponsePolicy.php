<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Event\Registration\Response;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegistrationResponsePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list the registration responses.
     */
    public function list(User $user, Event $event)
    {
        //
        return $user->hasAccess("event.{$event->uri}.response.list");
    }

    /**
     * Determine whether the user can view the registration response.
     */
    public function view(User $user, Response $response)
    {
        //
        return $user->hasAccess("event.{$response->event->uri}.response.view");
    }

    /**
     * Determine whether the user can create registration responses.
     */
    public function create(User $user, Event $event)
    {
        //
        return $user->hasAccess("event.{$event->uri}.response.create");
    }

    /**
     * Determine whether the user can update the registration response.
     */
    public function update(User $user, Response $response)
    {
        //
        return $user->hasAccess("event.{$response->event->uri}.response.update");
    }

    /**
     * Determine whether the user can delete the registration response.
     */
    public function delete(User $user, Response $response)
    {
        //
        return $user->hasAccess("event.{$response->event->uri}.response.delete");
    }
}
