<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Payment;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can list the payments.
     */
    public function list(User $user, Event $event)
    {
        //
        return $user->hasAccess("event.{$event->uri}.payment.list");
    }
    
    /**
     * Determine whether the user can view the payment.
     */
    public function view(User $user, Payment $payment)
    {
        //
        $event = $payment->event;
        return $user->hasAccess("event.{$event->uri}.payment.view");
    }

    /**
     * Determine whether the user can create payments.
     */
    public function create(User $user, Event $event)
    {
        //
        return $user->hasAccess("event.{$event->uri}.payment.create");
    }

    /**
     * Determine whether the user can update the payment.
     */
    public function update(User $user, Payment $payment)
    {
        //
        $event = $payment->event;
        return $user->hasAccess("event.{$event->uri}.payment.update");
    }

    /**
     * Determine whether the user can delete the payment.
     */
    public function delete(User $user, Payment $payment)
    {
        //
        $event = $payment->event;
        return $user->hasAccess("event.{$event->uri}.payment.delete");
    }
}
