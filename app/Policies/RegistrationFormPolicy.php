<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Event\Registration\Form;
use Illuminate\Auth\Access\HandlesAuthorization;

class RegistrationFormPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the registration form.
     */
    public function view(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.form.view");
    }

    /**
     * Determine whether the user can create registration forms.
     */
    public function create(User $user, Event $event)
    {
        //
        return $user->hasAccess("event.{$event->uri}.form.create");
    }

    /**
     * Determine whether the user can update the registration form.
     */
    public function update(User $user, Form $form)
    {
        //
        $event = $form->event;
        return $user->hasAccess("event.{$event->uri}.form.update");
    }

    /**
     * Determine whether the user can delete the registration form.
     */
    public function delete(User $user, Form $form)
    {
        //
        $event = $form->event;
        return $user->hasAccess("event.{$event->uri}.form.delete");
    }
}
