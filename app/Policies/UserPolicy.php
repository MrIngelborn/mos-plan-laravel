<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     */
    public function view(User $user, User $user2)
    {
        //
        if ($user == $user2) {
            return true;
        }
        return $user->hasAccess('user.view');
    }

    /**
     * Determine whether the user can create users.
     */
    public function create(?User $user)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can update the user.
     */
    public function update(User $user, User $model)
    {
        //
        if ($user == $model) {
            return true;
        }
        return $user->hasAccess('user.update');
    }

    /**
     * Determine whether the user can delete the user.
     */
    public function delete(User $user, User $model)
    {
        //
        if ($user == $model) {
            return true;
        }
        return $user->hasAccess('user.delete');
    }
    
    /**
     * Determine whether the user can list all the users.
     */
    public function list(User $user)
    {
        return $user->hasAccess('user.list');
    }
    
    /**
     * Determine whether the user can change roles of the user.
     */
    public function roles(User $user)
    {
        return $user->hasAccess('user.roles');
    }
}
