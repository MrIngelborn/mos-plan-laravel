<?php

namespace App\Policies;

use App\Models\Event;
use App\Models\User;
use App\Models\Checkin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CheckinPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the checkin.
     */
    public function list(User $user, Event $event)
    {
        return $user->hasAccess("event.{$event->uri}.checkin.list");
    }

    /**
     * Determine whether the user can view the checkin.
     */
    public function view(User $user, Checkin $checkin)
    {
        return $user->hasAccess("event.{$checkin->event->uri}.checkin.list");
    }

    /**
     * Determine whether the user can create checkins.
     */
    public function create(User $user, Event $event)
    {
        
        return true;
        //return $user->hasAccess("event.{$event->uri}.checkin.create");
    }

    /**
     * Determine whether the user can update the checkin.
     */
    public function update(User $user, Checkin $checkin)
    {
        return $user->hasAccess("event.{$checkin->event->uri}.checkin.update");
    }

    /**
     * Determine whether the user can delete the checkin.
     */
    public function delete(User $user, Checkin $checkin)
    {
        return $user->hasAccess("event.{$checkin->event->uri}.checkin.delete");
    }
}
