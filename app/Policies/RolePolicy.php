<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     */
    public function list(User $user)
    {
        return $user->hasAccess('role.view');
    }
    
    /**
     * Determine whether the user can view the role.
     */
    public function view(User $user, Role $role)
    {
        return $user->hasAccess('role.view');
    }

    /**
     * Determine whether the user can create roles.
     */
    public function create(User $user)
    {
        return $user->hasAccess('role.create');
    }

    /**
     * Determine whether the user can update the role.
     */
    public function update(User $user, Role $role)
    {
        return $user->hasAccess('role.update');
    }

    /**
     * Determine whether the user can delete the role.
     */
    public function delete(User $user, Role $role)
    {
        return $user->hasAccess('role.delete');
    }
}
