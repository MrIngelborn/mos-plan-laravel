<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use App\Models\Checkin;
use App\Models\Event;
use App\Models\Payment;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\Event\Registration\Form;
use App\Models\Event\Registration\Response;
use App\Models\Role;
use App\Models\User;

use App\Policies\CheckinPolicy;
use App\Policies\EventPolicy;
use App\Policies\PaymentPolicy;
use App\Policies\ProductPolicy;
use App\Policies\PurchasePolicy;
use App\Policies\RegistrationFormPolicy;
use App\Policies\RegistrationResponsePolicy;
use App\Policies\RolePolicy;
use App\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Checkin::class => CheckinPolicy::class,
        Event::class => EventPolicy::class,
        Payment::class => PaymentPolicy::class,
        Product::class => ProductPolicy::class,
        Purchase::class => PurchasePolicy::class,
        Form::class => RegistrationFormPolicy::class,
        Response::class => RegistrationResponsePolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
    ];
}
