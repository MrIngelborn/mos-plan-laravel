<?php

namespace App\Models;

use App\Models\Event\Registration\Response;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'ssn',
        'phone1',
        'phone2',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'last_renewed' => 'date',
    ];

    /**
    * The events that the user has signed up to
    */
    public function events()
    {
        return $this->belongsToMany(Event::class)->withPivot('barcode_id');
    }

    public function eventSeats()
    {
        return $this->belongsTo(EventSeat::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function eventRegistrations()
    {
        return $this->hasMany(Response::class);
    }

    public function hasRole(string $roleSlug): bool
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }

    public function isAdmin(): bool
    {
        return $this->hasRole('admin');
    }

    public function hasAccess(string $permission): bool
    {
        foreach($this->roles as $role) {
            if ($role->hasAccess($permission)) return true;
        }
        return false;
    }
}
