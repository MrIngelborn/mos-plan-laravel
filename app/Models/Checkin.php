<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Checkin extends Model
{
    public $table = 'checkin_events';
    const view = 'view_checkin_status';
    public $timestamps = false;
    
    /**
    * The users that has signed up to the event
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
    
    public static function getStatus(Event $event, User $user)
    {
        $time = Checkin::where(['event_id' => $event->id, 'user_id' => $user->id])->max('time');
        $checkin_event = Checkin::where(['event_id' => $event->id, 'user_id' => $user->id, 'time' => $time])->first();
        if ($checkin_event == null) return null;
        $status = $checkin_event->check_in;
        return $status;
    }
    
    /**
    * All statuses for the given event
    */
    public static function getStatuses(Event $event)
    {
        $view = Checkin::view;
        return DB::select("SELECT * FROM $view WHERE event_id = ?", [$event->id]);
    }
}

