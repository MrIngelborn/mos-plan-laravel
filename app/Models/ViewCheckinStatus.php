<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewCheckinStatus extends Model
{
    protected $table = 'view_checkin_status';
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
