<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'value'];
    
    public static function get(string $id): ?string
    {
        $conf = Config::find('event_id');
        return $conf ? $conf->value : null;
    }
    
    public static function set(string $id, string $value): ?string
    {
        return Config::updateOrCreate(
            ['id' => $id],
            ['value' => $value]
        );
    }
}
