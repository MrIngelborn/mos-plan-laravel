<?php

namespace App\Models\Event\Registration;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $table = 'event_registration_form_responses';
    
    public function values()
    {
        return $this->hasMany(ResponseValue::class, 'response_id');
    }
    
    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
