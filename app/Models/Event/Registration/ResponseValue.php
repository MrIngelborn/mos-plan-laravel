<?php

namespace App\Models\Event\Registration;

use Illuminate\Database\Eloquent\Model;

class ResponseValue extends Model
{
    protected $table = 'event_registration_form_response_values';
    
    public function response()
    {
        return $this->belongsTo(Response::class, 'response_id');
    }
    
    public function input()
    {
        return $this->belongsTo(FormInput::class, 'input_id');
    }
}
