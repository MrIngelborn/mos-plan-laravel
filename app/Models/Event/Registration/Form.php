<?php

namespace App\Models\Event\Registration;

use App\Models\Event;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Date;

/**
 * @property Date|null $registration_opens_at
 * @property Date|null $registration_closes_at
 *
 * @property Form $event
 * @property Collection<FormInput> $inputs
 */
class Form extends Model
{
    protected $table = 'event_registration_forms';
    protected $guarded = ['id', 'event_id'];

    public function inputs()
    {
        return $this->hasMany(FormInput::class, 'form_id');
    }

    public function responses()
    {
        return $this->hasMany(Response::class, 'form_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    /**
    * Compares the registration open/close timespamps with the current time
    * to check for availablity.
    *
    * @returns  -1 if registration has not opened yet
    *           0 if registation is open
    *           1 if registration has closed
    */
    public function registrationAvailablity(): int
    {
        $open_time = strtotime($this->registration_opens_at);
        $close_time = strtotime($this->registration_closes_at);
        $current_time = now()->timestamp;

        if (isset($this->registration_opens_at) AND $open_time > $current_time) return -1;
        if (isset($this->registration_closes_at) AND $close_time < $current_time) return 1;
        return 0;
    }
    public function isRegistrationOpen(): bool
    {
        return $this->registrationAvailablity() == 0;
    }
}
