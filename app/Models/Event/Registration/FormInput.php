<?php

namespace App\Models\Event\Registration;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $label
 * @property string $type
 * @property string $placeholder
 * @property int|null $price
 * @property int $limit
 * @property int $order
 * @property boolean $oncard
 */
class FormInput extends Model
{
    use HasFactory;

    protected $table = 'event_registration_form_inputs';

    protected $fillable = [
        'name',
        'label',
        'type',
        'placeholder',
        'price',
        'limit',
        'order',
        'oncard',
    ];

    public function oncard(): Attribute
    {
        return Attribute::make(
            get: fn (int $value) => (bool) $value,
        );
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

}
