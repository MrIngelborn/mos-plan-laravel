<?php

namespace App\Models;

use App\Models\Event\Registration\Form;
use Database\Factories\EventFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Date;

/**
 * @property string name
 * @property int $max_places
 * @property int $max_seats
 * @property int $entry_place
 * @property string $description
 * @property string $barcode_prefix
 * @property string $uri
 * @property Date $start_time
 * @property Date $end_time
 * @property int $max_dept
 *
 * @property Form $form
 */
class Event extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = ['id'];

    /**
    * Get the value of the model's route key.
    *
    * @return mixed
    */
    public function getRouteKey()
    {
        return $this->uri;
    }

    /**
    * The users that has signed up to the event
    */
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('barcode_id');
    }

    public function seats()
    {
        return $this->hasMany(EventSeat::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function form()
    {
        return $this->hasOne(Form::class);
    }

    public function isRegistrationOpen()
    {
        return $this->registrationAvailablity() == 0;
    }

    public function registrationAvailablity()
    {
        return isset($this->registrationForm) ? $this->registrationForm->registrationAvailablity() : -2;
    }
}
