<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewUserEconomy extends Model
{
    protected $table = 'view_user_economy';
}
