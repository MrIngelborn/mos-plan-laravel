<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'slug', 'permissions',
    ];
    protected $casts = [
        'permissions' => 'array',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user');
    }

    public function hasAccess(string $permission) {
        foreach ($this->permissions as $key => $value) {
            if (fnmatch($key, $permission)) return $value;
        }
        return false;
    }
}
