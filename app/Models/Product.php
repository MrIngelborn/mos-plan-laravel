<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
    
    public function purchases()
    {
        return $this->belongsToMany(Event::class)
                ->withPivot('amount');
    }
}
