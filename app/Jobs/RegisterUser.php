<?php

namespace App\Jobs;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\Types\Boolean;

class RegisterUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        Log::debug('RegisterUser@__construct: Creating registration job for user '.$user->id);
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::debug('RegisterUser@handle: Handling registration job for user '.$this->user->id);

        if ($this->user->last_renewed && $this->user->last_renewed->year == Carbon::now()->year) {
            Log::debug('RegisterUser@handle: User with id '.$this->user->id.' has already been registered');
            return;
        }

        if ($this->userIsMember()) {
            Log::debug('RegisterUser@handle: User with id '.$this->user->id.' is already a member.');
            $this->user->last_renewed = Carbon::now();
            $this->user->save();
            return;
        }

        $today = Carbon::now()->format('Y-m-d');
        $request = array(
            "api_key" => env('SVEROK_KEY'),
            "member" => array(
                "renewed" => $today,
                "socialsecuritynumber" => $this->user->ssn,
                "email" => $this->user->email,
                "phone1" => $this->user->phone1,
            )
        );

        $url = "https://ebas.sverok.se/apis/submit_member_with_lookup.json";

        $result = $this->curl($url, $request);

        if ($result->stored_member) {
            $this->user->last_renewed = $today;
            $this->user->save();
        }

    }

    private function userIsMember(): bool
    {
        $request = array(
            'request' => array(
                'action' => 'confirm_membership',
                'version' => '2015-06-01',
                'association_number' => env('SVEROK_F'),
                'api_key' => env('SVEROK_KEY'),
                'year_id' => Carbon::now()->year,
                'socialsecuritynumber' => $this->user->ssn,
            )
        );
        $url = 'https://ebas.sverok.se/apis/confirm_membership.json';

        $result = $this->curl($url, $request);

        return $result->response->member_found;

    }

    private function curl(string $url, array $request): object
    {
        $data_string = json_encode($request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        Log::debug('RegisterUser@curl: Curl response: '.$result);

        if ($result === false) {
            Log::error('RegisterUser@curl: Curl error: '.curl_error($ch));
        }

        curl_close($ch);

        return json_decode($result);
    }
}
