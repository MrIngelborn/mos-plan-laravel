<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateEvent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Get the event
        $event = $this->route('event');
        
        //var_dump($event); return true;
        
        // Check for authority
        return $event && $this->user()->can('update', $event);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $event = $this->route('event');
        
        return [
            'uri' => [
                'required',
                'max:20',
                'alpha_num',
                Rule::unique('events')->ignore($event->id),
            ],
            'name' => 'required|max:100',
            'entry_price' => 'required|integer|min:0',
            'barcode_prefix' => 'required|min:1|max:100',
            'description' => 'string|nullable',
        ];
    }
    
    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation()
    {
        // Trim all data
        return $this->all();
    }
}
