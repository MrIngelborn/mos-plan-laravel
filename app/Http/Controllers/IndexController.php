<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Event;
use App\Models\Role;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $adminbar = null;
        $admin = Auth::user();
        if ($admin) {
            if ($admin->can('list', User::class))
                $adminbar['Användare'] = route('users.index');
            if ($admin->can('list', Event::class))
                $adminbar['Event'] = route('events.index');
            if ($admin->can('list', Role::class))
                $adminbar['Roller'] = route('roles.index');
        }
        
        return view('index')->with('adminbar', $adminbar);                         
    }
}
