<?php

namespace App\Http\Controllers;

use App\Models\Config;
use App\Models\Event;
use App\Models\ViewUserEconomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     */
    public function show()
    {
        $event_id = Config::get('event_id');
        
        $event = Event::find($event_id);
        
        $user = Auth::user();
        $user_economy = ViewUserEconomy::where([
                'user_id' =>  Auth::id(),
                'event_id' => $event_id
        ])->first();
                
        return view('user.dashboard')
                ->with('user', $user)
                ->with('event', $event)
                ->with('economy', $user_economy);
    }
    
    public function purchases()
    {
        $event_id = Config::get('event_id');
        $event = Event::find($event_id);
        $purchases = Auth::user()->purchases()->where('event_id', $event_id)->get();
        
        $header = "Köp i butik under {$event->name}";
        
        $table = (object)[];
        
        $table->headers = ['Datum', 'Produkt', 'Antal', 'Pris', 'Summa'];
        
        $table->rows = [];
        foreach ($purchases as $purchase) {
            foreach ($purchase->products as $product) {
                $row = [];
                $row[] = $purchase->created_at->timezone('CET');
                $row[] = $product->name;
                $row[] = $product->pivot->amount;
                $row[] = $product->price;
                $row[] = $product->price * $product->pivot->amount;
                $table->rows[] = $row;
            }
        }
        
        return view('general.table')
                ->with('header', $header)
                ->with('table', $table);
    }

    /**
     * Show the form for editing the current user.
     */
    public function edit() {
        $user = Auth::user();
        $userController = new UserController();
        return $userController->edit($user);
    }
}
