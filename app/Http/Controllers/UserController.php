<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $this->authorize('list', User::class);

        $users = User::get();

        $table = (object)[];
        $table->headers = [
            'Id',
            'Namn',
            'Epost',
            'Personnummer',
            'Telefon',
            'Alt. telefon',
            'Senast medlemsreg.',
            'Registrering'
        ];
        $table->rows = [];
        foreach ($users as $user) {
            $row = [];
            $id_cell = (object)[];
            $id_cell->link = route('users.show', $user->id);
            $id_cell->value = $user->id;
            $row[] = $id_cell;
            $row[] = $user->name;
            $row[] = $user->email;
            $row[] = $user->ssn;
            $row[] = $user->phone1;
            $row[] = $user->phone2;
            $row[] = $user->last_renewed;
            $row[] = $user->created_at;
            $table->rows[] = $row;
        }

        $header = "Användare";

        return view('general.table')
            ->with('header', $header)
            ->with('table', $table);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        //

    }

    public function store(Request $request)
    {
        //
    }
    public function show(User $user)
    {
        $this->authorize('view', $user);

        return view('user.show')
            ->with('user', $user);
    }

    public function edit(User $user)
    {
        $this->authorize('update', $user);

        if (Gate::allows('roles', $user)) {
            $roles = Role::get();

            return view('user.edit')
            ->with('user', $user)
            ->with('roles', $roles);

        }

        return view('user.edit')
            ->with('user', $user);
    }

    public function update(Request $request, User $user)
    {
        // Authorize request
        $this->authorize('update', $user);

        // Validate request
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'roles' => 'array'
        ]);

        if (isset($validated['roles'])) {
            // Setup new roles
            $new_roles = $validated['roles'];

            // Remove already associated roles
            foreach($user->roles as $role) {
                if (in_array($role->id, $new_roles)) {
                    unset($new_roles[$role->id]);
                }
                else {
                    $user->roles()->detach($role->id);
                }
            }

            // Add missing roles
            foreach ($new_roles as $role) {
                $user->roles()->attach($role);
            }

        }

        $user->name = $validated['name'];
        $user->email = $validated['email'];

        $user->save();

        if ($user->id == Auth::user()->id) {
            return redirect(route('home.index'));
        }

        return redirect(route('users.index'));
    }

    public function destroy(User $user)
    {
        $this->authorize('delete', $user);

        $user->delete();

        if ($user->id == Auth::user()->id) {
            Auth::logout();
            return redirect(route('index'));
        }

        return redirect(route('users.index'));
    }
}
