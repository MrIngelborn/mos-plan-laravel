<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Event\Registration\Form;
use App\Models\Event\Registration\FormInput;
use Illuminate\Http\Request;

class FormInputController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    private function createForm(Event $event, ?FormInput $formInput = null): object
    {
        $action = $formInput ? 'update' : 'store';
        $formData = (object)[
            'inputs' => [
                $this->createFormInput($formInput, 'oncard', 'hidden', [ 'value' => 0 ]),
                $this->createFormInput($formInput, 'name', 'text', [ 'required' => true ]),
                $this->createFormInput($formInput, 'label', 'text', [ 'required' => true ]),
                $this->createFormInput($formInput, 'type', 'text', [ 'required' => true ]),
                $this->createFormInput($formInput, 'placeholder', 'text', [ 'required' => true ]),
                $this->createFormInput($formInput, 'price', 'number'),
                $this->createFormInput($formInput, 'limit', 'number', [ 'required' => true ]),
                $this->createFormInput($formInput, 'order', 'number', [ 'required' => true ]),
                $this->createFormInput($formInput, 'oncard', 'checkbox', [ 'value' => 1 ]),
                (object)[
                    'name' => '',
                    'type' => 'submit',
                    'value' => __("form.$action"),
                ],
            ]
        ];
        if ($formInput) {
            $formData->method = 'PATCH';
            $formData->action = route("events.form.input.update", [$event, $formInput]);
        } else {
            $formData->action = route("events.form.input.store", $event);
        }
        return $formData;
    }

    private function createFormInput(?FormInput $formInput, string $name, string $type, array $extra = []): object
    {
        return
            (object)array_merge([
                'name' => $name,
                'label' => __("event.form.input.$name"),
                'type' => $type,
                'value' => $formInput?->$name,
            ], $extra);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Event $event)
    {
        $this->authorize('update', $event->form);

        $title = __('event.form.input.create');
        $form = $this->createForm($event);

        return view('general.form')
            ->with('title', $title)
            ->with('form', $form);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Event $event)
    {
        $this->authorize('update', $event->form);

        $validated = $request->validate([
            'name' => 'required|unique:event_registration_form_inputs',
            'label' => 'required|string',
            'type' => 'required|string',
            'placeholder' => 'required|string',
            'price' => 'nullable|int',
            'limit' => 'required|int',
            'order' => 'required|int',
            'oncard' => 'bool',
        ]);

        $event->form->inputs()->create($validated);

        return redirect(route('events.form.show', $event));
    }

    /**
     * Display the specified resource.
     */
    public function show(Form $form, FormInput $formInput)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Form $form, FormInput $formInput)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Form $form, FormInput $formInput)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Form $form, FormInput $formInput)
    {
        //
    }
}
