<?php

namespace App\Http\Controllers\Event;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;

class EconomyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Handle the incoming request.
     */
    public function __invoke(Event $event)
    {
        // Authorize
        $this->authorize('list', [Purchase::class, $event]);
        
        $responses = $event->registrationForm?->responses ?? array();
        
        // Generate table
        $table = (object)[];
        
        $table->headers = [
            'Produkt', 'Pris', 'Antal', 'Summa'
        ];
        
        $sum_row = ['TOTAL SUMMA', '', '', 0];
        
        // Entry cost
        $entry_cost_row = [];
        $entry_cost_row[] = 'Entrékostnad';
        $entry_cost_row[] = $event->entry_price . ' kr';
        $entry_cost_row[] = count($responses);
        $entry_cost_row[] = count($responses) * $event->entry_price . ' kr';
        $table->rows[] = $entry_cost_row;
        
        $sum_row[3] += count($responses) * $event->entry_price;
        
        // Add space
        $table->rows[] = ['', '', '', ''];
        
        // Extra event options
        $sub_sum_row = ['SUMMA Extra', '', 0, 0];
        $inputs = $event->registrationForm?->inputs()->where('price', '>', '0')->get() ?? [];
        foreach ($inputs as $input) {
            $row = [];
            $row[0] = $input->label;
            $row[1] = $input->price . ' kr';
            $count = 0;
            foreach ($responses as $response) {
                $count += $response->values()->where('input_id', $input->id)->count();
            }
            $row[2] = $count;
            $row[3] = $count * $input->price . ' kr';
            
            // Add to sum row
            $sub_sum_row[2] += $count;
            $sub_sum_row[3] += $count * $input->price;
            
            $table->rows[] = $row;
        }
        $sum_row[3] += $sub_sum_row[3];
        $sub_sum_row[3] .= ' kr';
        $table->rows[] = $sub_sum_row;
        
        // Add space
        $table->rows[] = ['', '', '', ''];
        
        // Purcahses
        $sub_sum_row = ['SUMMA Köp', '', 0, 0];
        foreach($event->products as $product) {
            $row = [];
            $row[0] = $product->name;
            $row[1] = $product->price . ' kr';
            $count = 0;
            foreach ($product->purchases as $purcahse) {
                $count += $purcahse->pivot->amount;
            }
            $row[2] = $count;
            $row[3] = $count * $product->price . ' kr';
            
            // Add to sum row
            $sub_sum_row[2] += $count;
            $sub_sum_row[3] += $count * $product->price;
            
            $table->rows[] = $row;
        }
        $sum_row[3] += $sub_sum_row[3];
        $sub_sum_row[3] .= ' kr';
        $table->rows[] = $sub_sum_row;
        
        // Add space
        $table->rows[] = ['', '', '', ''];
        
        // SUM
        $sum_row[3] .= ' kr';
        $table->rows[] = $sum_row;
        
        //Heading
        $header = "Inkomster under {$event->name}";
        
        return view('general.table')
            ->with('table', $table)
            ->with('header', $header);
    }
}
