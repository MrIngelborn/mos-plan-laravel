<?php
namespace App\Http\Controllers\Event;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

use App\Http\Controllers\Controller;

use App\Models\Event;
use App\Models\User;
use App\Models\Event\Registration\Form as Form;
use App\Models\Event\Registration\FormInput as Input;
use App\Models\Event\Registration\Response as Response;
use App\Models\Event\Registration\ResponseValue as ResponseValue;

/**
 * Allow admins to list, create and update responses to the signup form
 */
class ResponseController extends Controller
{
    private MessageBag $formErrors;
    
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->formErrors = new MessageBag;
    }
    
    public function index(Event $event)
    {
        // Authorize
        $this->authorize('list', [Response::class, $event]);
        
        $form = $event->registrationForm;
        $inputs = $form?->inputs ?? [];
        $responses = $form?->responses ?? [];
        
        $table = (object)[];
        $table->headers = [
            '#', 'Användare', 'E-post'
        ];
        foreach ($inputs as $input) {
            $table->headers[] = $input->label;
        }
        $table->headers[] = 'Delete?';
        
        $table->rows = [];
        $id = 1;
        foreach ($responses as $response) {
            $row = [];
            $row[] = $id++;
            $row[] = $response->user->name;
            $row[] = $response->user->email;
            
            foreach ($inputs as $input) {
                $value = $response->values()->where('input_id', $input->id)->first();
                $row[] = $value ? $value->value : null;
            }
            
            $delete = (object)[];
            $delete->delete = true;
            $delete->link = route('events.responses.destroy', [$event, $response->user->id]);
            $delete->value = 'Delete';
            $row[] = $delete;
            
            $table->rows[] = $row;
        }
        
        $header = "Anmälningar för {$event->name}";
        
        $adminbar = [];
        if (Auth::user()->can('create', [Response::class, $event]))
            $adminbar['Ny anmälan'] = route('events.responses.create', $event);
        
        return view('general.table')
            ->with('header', $header)
            ->with('table', $table)
            ->with('adminbar', $adminbar);
    }
    
    public function create(Event $event)
    {
        // Authorize
        $this->authorize('create', [Response::class, $event]);
        
        $form = $event->registrationForm;
        
        // Check if form exists
        if ($form == null) {
            $this->formErrors->add('form', 'Det finns inget registreringsformulär för det här eventet');
        }
        
        if ($form != null) {
            $inputs = $form->inputs()->orderBy('order', 'asc')->get();
                
            foreach ($inputs as $input) {
                $input->value = "";
                $input->disabled = "";
                if ($input->limit > 0) {
                    $count = 0;
                    $responses = Response::where('form_id', $form->id)->get();
                    foreach ($responses as $response) {
                        $count += $response->values()->where('input_id', $input->id)->get()->count();
                    }
                    if ($count >= $input->limit) $input->disabled = "disabled";
                }
            }
            
            $users = User::get()->keyBy('id');
            foreach($form->responses as $response) {
                $users->forget($response->user->id);
            }
            
        }
        $form_action = route('events.responses.store', $event);
        
        return view('event.registration_new')
            ->with('event_name', $event->name)
            ->with('event_price', $event->entry_price)
            ->with('users', $users ?? [])
            ->with('inputs', $inputs ?? [])
            ->with('form_action', $form_action)
            ->with('form_errors', $this->formErrors);
    }
    
    public function store(Request $request, Event $event)
    {
        $form = $event->registrationForm;
        
        // Authorize
        $this->authorize('create', [Response::class, $event]);
        
        $validated = $request->validate([
            'user' => 'required'
        ]);
        
        // Get user
        $user = User::find($validated['user']);
        
        // Check if form exists
        if ($form == null) {
            $this->formErrors->add('form', 'Det finns inget registreringsformulär för det här eventet');
            return $this->show($request, $event);
        }
        // See if user already submitted a form
        else if (Response::where('form_id', $form->id)->where('user_id', $user->id)->exists()) {
            $this->formErrors->add('form', "{$user->name} har redan anmält dig till det här arrangemanget");
            return $this->edit($request, $event, $form);
        }
        
        // Create a new response
        $response = new Response;
        $response->user()->associate($user);
        $response->form()->associate($form);
        $response->save();
        
        // Iterate through the form responces
        foreach ($request->except(['_token', 'policy']) as $key=>$value) {
            if ($value == NULL) continue;
            
            // Get the input associated with the form response
            $input = Input::where('name', $key)->first();
            
            if ($input == null) continue;
            
            // Create a new form response value
            $response_value = new ResponseValue;
            $response_value->input()->associate($input);
            $response_value->value = $value;
            
            $response->values()->save($response_value);
        }
        
        // Create association between user and event
        $barcode_id = 1;
        foreach ($event->users as $eventUser) {
            if ($eventUser->pivot->barcode_id >= $barcode_id) {
                $barcode_id = $eventUser->pivot->barcode_id + 1;
            }
        }
        $event->users()->save($user, ['barcode_id' => $barcode_id]);
        
        return redirect(route('events.responses.create', $event));
    }
    
    public function destroy(Event $event, $user_id)
    {
        $form = $event->registrationForm;
        
        // Authorize
        $this->authorize('delete', [Response::class, $event]);
        
        // Delete registration
        $event->users()->detach($user_id);
        Response::where('form_id', $form->id)->where('user_id', $user_id)->delete();
        
        return redirect(route('events.responses.index', $event));
    }
    
    public function edit(Request $request, Event $event, $registration)
    {
        // Authorize
        $this->authorize('update', [Response::class, $event]);
    }
    
    public function update(Request $request, Event $event, $registration)
    {
        // Authorize
        $this->authorize('update', [Response::class, $event]);
    }
}