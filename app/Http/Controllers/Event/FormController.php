<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Event\Registration\Form;
use App\Models\Event\Registration\FormInput;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    private function createForm($action, Event $event): object
    {
        $form = $event->form;
        $formData = (object) [
            'action' => route("events.form.$action", $event),
            'inputs' => [
                (object)[
                    'name' => 'registration_opens_at',
                    'label' => __('event.form.registration_opens_at'),
                    'type' => 'datetime-local',
                    'max' => '9999-12-31T23:59:59',
                    'required' => false,
                    'value' => $form?->registration_opens_at,
                ],
                (object)[
                    'name' => 'registration_closes_at',
                    'label' => __('event.form.registration_closes_at'),
                    'type' => 'datetime-local',
                    'max' => '9999-12-31T23:59:59',
                    'required' => false,
                    'value' => $form?->registration_closes_at,
                ],
                (object)[
                    'name' => '',
                    'type' => 'submit',
                    'value' => __("form.$action"),
                ],
            ]
        ];
        if ($action == 'update') {
            $formData->method = 'PATCH';
        }
        return $formData;
    }

    private function createInputTable(?Form $form): object {
        $table = (object) [];
        $attributes = new Collection((new FormInput())->getFillable());

        $table->headers = $attributes->map(
            fn($attribute) => __("event.form.input.$attribute")
        );

        $table->rows = $form?->inputs->map(
            fn($input) => $attributes->map(
                fn($attribute) => $input->getAttributeValue($attribute)
            )
        );

        return $table;
    }

    /**
     * Show the form for creating a new resource.
     * @throws AuthorizationException
     */
    public function create(Event $event)
    {
        $this->authorize('create', [Form::class, $event]);

        $title = __( 'event.form.create');
        $form = $this->createForm('store', $event);

        return view('general.form')
            ->with('title', $title)
            ->with('form', $form);
    }

    /**
     * Store a newly created resource in storage.
     * @throws AuthorizationException
     */
    public function store(Request $request, Event $event)
    {
        $this->authorize('create', [Form::class, $event]);

        $validated = $request->validate([
            'registration_opens_at' => 'nullable|date',
            'registration_closes_at' => 'nullable|date',
        ]);

        $event->form()->create($validated);

        return redirect(route('events.form.show', $event));
    }

    /**
     * Display the specified resource.
     * @throws AuthorizationException
     */
    public function show(Event $event)
    {
        $this->authorize('view', [Form::class, $event]);

        $form = $event->form;

        $adminbar = null;
        $user = Auth::user();
        if ($form) {
            if ($user->can('update', $form)) {
                $adminbar[__('event.form.edit')] = route('events.form.edit', $event);
            }
            if ($user->can('update', $form)) {
                $adminbar[__('event.form.input.create')] = route('events.form.input.create', $event);
            }
        } else {
            if ($user->can('create', [Form::class, $event])) {
                $adminbar[__('event.form.create')] = route('events.form.create', $event);
            }
        }

        $table = $this->createInputTable($form);

        return view('event.form.show')
            ->with('adminbar', $adminbar)
            ->with('event', $event)
            ->with('form', $form)
            ->with('table', $table);
    }

    /**
     * Show the form for editing the specified resource.
     * @throws AuthorizationException
     */
    public function edit(Event $event)
    {
        $this->authorize('update', $event->form);

        $title = __( 'event.form.edit');
        $form = $this->createForm('update', $event);

        return view('general.form')
            ->with('title', $title)
            ->with('form', $form);
    }

    /**
     * Update the specified resource in storage.
     * @throws AuthorizationException
     */
    public function update(Request $request, Event $event)
    {
        $this->authorize('update', $event->form);

        $validated = $request->validate([
            'registration_opens_at' => 'nullable|date',
            'registration_closes_at' => 'nullable|date',
        ]);

        $event->form()->update($validated);

        return redirect(route('events.form.show', $event));
    }

    /**
     * Remove the specified resource from storage.
     * @throws AuthorizationException
     */
    public function destroy(Event $event)
    {
        $this->authorize('delete', $event->form);
    }
}
