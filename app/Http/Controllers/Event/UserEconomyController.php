<?php

namespace App\Http\Controllers\Event;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\ViewUserEconomy;
use App\Models\Purchase;
use App\Models\User;

class UserEconomyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Event $event)
    {
        // Authorize
        $this->authorize('list', [Purchase::class, $event]);
        
        $userEconomies = ViewUserEconomy::where('event_id', $event->id)->get();
        
        $table = (object)[];
        
        $table->headers = [
            'Användare', 'Entré', 'Extra', 'Caféköp', 'Summa', 'Betalat', 'Skuld'
        ];
        
        $table->rows = [];
        
        $sum_row = ['SUM', 0, 0, 0, 0, 0, 0];
        
        foreach ($userEconomies as $userEconomy) {
            $row = [];
            $row[] = User::find($userEconomy->user_id)->name;
            $row[] = $userEconomy->entry_price . 'kr';
            $row[] = $userEconomy->event_extra . 'kr';
            $row[] = $userEconomy->event_purchases . 'kr';
            $row[] = $userEconomy->entry_price + $userEconomy->event_extra + $userEconomy->event_purchases . 'kr';
            $row[] = $userEconomy->payed . 'kr';
            $row[] = $userEconomy->debt . 'kr';
            $table->rows[] = $row;
            
            $sum_row[1] += $userEconomy->entry_price;
            $sum_row[2] += $userEconomy->event_extra;
            $sum_row[3] += $userEconomy->event_purchases;
            $sum_row[4] += $userEconomy->entry_price + $userEconomy->event_extra + $userEconomy->event_purchases;
            $sum_row[5] += $userEconomy->payed;
            $sum_row[6] += $userEconomy->debt;
        }
        
        $sum_row[1] .= ' kr';
        $sum_row[2] .= ' kr';
        $sum_row[3] .= ' kr';
        $sum_row[4] .= ' kr';
        $sum_row[5] .= ' kr';
        $sum_row[6] .= ' kr';
        
        $table->rows[] = ['', '', '', '', '', '', ''];
        $table->rows[] = $sum_row;
        
        $header = "Användarekonomi under {$event->name}";
        
        return view('general.table')
            ->with('table', $table)
            ->with('header', $header);
    }
}
