<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Models\Event;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index(Event $event)
    {
        // Check for authority
        $this->authorize('list', [Payment::class, $event]);
        
        // Get all payments
        $payments = Payment::where('event_id', $event->id)->get();
        
        $table = (object)[];
        $table->headers = [
            'Tid', 'Användare', 'Summa', 'Delete?'
        ];
        $table->rows = [];
        
        $sum = 0;
        foreach ($payments as $payment) {
            $row = [];
            $row[0] = $payment->created_at;
            $row[1] = $payment->user->name;
            $row[2] = $payment->amount;
            $sum += $payment->amount;
            $row[3] = (object)[];
            $row[3]->delete = true;
            $row[3]->link = route('events.payments.destroy', [$event, $payment]);
            $row[3]->value = 'Delete';
            $table->rows[] = $row;
        }
        
        $table->rows[] = [
            '', 'SUM', $sum, ''
        ];
        
        $header = "Betalningar under {$event->name}";
        
        $adminbar = [];
        if (Auth::user()->can('create', [Payment::class, $event]))
            $adminbar['Ny Betalning'] = route('events.payments.create', $event);
        
        return view('general.table')
            ->with('table', $table)
            ->with('header', $header)
            ->with('adminbar', $adminbar);
    }

    public function create(Event $event)
    {
        // Check for authority
        $this->authorize('create', [Payment::class, $event]);
        
        // Get users
        $users = $event->users;
        
        return view('event.payments.create')
            ->with('event', $event)
            ->with('users', $users);
    }

    public function store(Request $request, Event $event)
    {
        // Check for authority
        $this->authorize('create', [Payment::class, $event]);
        
        // Validate request
        $validated = $request->validate([
            'user' => 'required|exists:users,id',
            'amount' => 'required|integer'
        ]);
        
        $user = User::find($validated['user']);
        
        $payment = new Payment;
        $payment->amount = $validated['amount'];
        $payment->user()->associate($user);
        $payment->event()->associate($event);
        $payment->save();
        
        return redirect(route('events.payments.create', $event));
    }

    public function destroy(Event $event, Payment $payment)
    {
        // Check for authority
        $this->authorize('delete', $payment);
        
        $payment->delete();
        
        return redirect(route('events.payments.index', $event));
    }
}
