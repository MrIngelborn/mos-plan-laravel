<?php

namespace App\Http\Controllers\Event;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Event\Registration\Form;
use App\Models\Event\Registration\FormInput;
use App\Models\Event\Registration\Response;
use App\Models\Event\Registration\ResponseValue;

/**
 * Allow users to signup to and edit their signup response
 */
class SignupController extends Controller
{

    private $form_errors;

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->form_errors = new MessageBag;
        //View::share('form_errors', $this->form_errors);
    }

    private function getView(Event $event, $inputs = null): \Illuminate\Contracts\View\View
    {
        return view('event.signup')
            ->with('event_name', $event->name)
            ->with('event_price', $event->entry_price)
            ->with('inputs', $inputs)
            ->with('form_errors', $this->form_errors);
    }

    public function create(Request $request, Event $event) {
        return $this->show($request, $event);
    }

    public function show(Request $request, Event $event)
    {
        $form = $event->form;

        if ($form == null) {
            $this->form_errors->add('form', 'Det finns inget registreringsformulär för det här eventet');
            return $this->getView($event);
        }

        $hasPreviousResponse = Response::where('form_id', $form->id)->where('user_id', Auth::id())->exists();
        if ($hasPreviousResponse) {
            return $this->edit($request, $event, $form);
        }

        if (!$form->isRegistrationOpen()) {
            $this->form_errors->add('form', 'Registrering är inte tillgängligt för tillfället');
            return $this->getView($event);
        }

        $no_responses = Response::where('form_id', $form?->id)->get()->count();
        if ($event->max_seats != null and $event->max_seats <= $no_responses) {
            $this->form_errors->add('form', 'Det finns tyvärr inga fler platser.');
            return $this->getView($event);
        }

        $inputs = $form->inputs()->orderBy('order', 'asc')->get();

        foreach ($inputs as $input) {
            $input->value = "";
            $input->disabled = "";
            if ($input->limit > 0) {
                $count = 0;
                $responses = Response::where('form_id', $form->id)->get();
                foreach ($responses as $response) {
                    $count += $response->values()->where('input_id', $input->id)->get()->count();
                }
                if ($count >= $input->limit)
                    $input->disabled = "disabled";
            }
        }

        return $this->getView($event, $inputs);
    }

    public function store(Request $request, Event $event)
    {
        $request->validate([
            'policy' => 'required'
        ]);
        $user = Auth::user();
        $form = $event->registrationForm;

        if ($form == null) {
            $this->form_errors->add('form', 'Det finns inget registreringsformulär för det här eventet');
            return $this->show($request, $event);
        }
        $hasPreviousResponse = Response::where('form_id', $form->id)->where('user_id', Auth::id())->exists();
        if ($hasPreviousResponse) {
            $this->form_errors->add('form', 'Du har redan anmält dig till det här arrangemanget');
            return $this->edit($request, $event, $form);
        }
        if (!$form->isRegistrationOpen()) {
            $this->form_errors->add('form', 'Registrering är inte tillgängligt för tillfället');
            return $this->show($request, $event);
        }
        $no_responses = Response::where('form_id', $form->id)->get()->count();
        if ($event->max_seats != null and $event->max_seats <= $no_responses) {
            $this->form_errors->add('form', 'Det finns tyvärr inga fler platser.');
            return $this->show($request, $event);
        }

        $response = new Response;
        $response->user()->associate($user);
        $response->form()->associate($form);
        $response->save();

        // Iterate through the form responces
        foreach ($request->except(['_token', 'policy']) as $key => $value) {
            if ($value == NULL)
                continue;

            // Get the input associated with the form response
            $input = FormInput::where('name', $key)->first();

            if ($input == null)
                continue;

            // Create a new form response value
            $response_value = new ResponseValue();
            $response_value->input()->associate($input);
            $response_value->value = $value;

            $response->values()->save($response_value);
        }

        // Create association between user and event
        $barcode_id = 1;
        foreach ($event->users as $eventUser) {
            if ($eventUser->pivot->barcode_id >= $barcode_id) {
                $barcode_id = $eventUser->pivot->barcode_id + 1;
            }
        }
        $event->users()->save($user, ['barcode_id' => $barcode_id]);

        return redirect(route('home.index'));
    }

    private function edit(Request $request, Event $event, Form $form)
    {
        $disabled = '';
        if (!$form->isRegistrationOpen()) {
            $this->form_errors->add('form', 'Registreringen är stängd. Du kan inte längre ändra din anmälan.');
            $disabled = 'disabled';
        }

        $user_response = Response::where('form_id', $form->id)->where('user_id', Auth::id())->first();

        /*       
        $inputs = Input::where('form_id', $form->id)->get();
        foreach ($inputs as $input) {
        $response_value = $response->values()->where('input_id', $input->id)->first();
        $response_values[$input->name] = $response_value == null ? null : $response_value->value;
        }
        */

        if ($form != null)
            $inputs = $form->inputs()->orderBy('order', 'asc')->get();

        foreach ($inputs as $input) {
            $user_response_value = $user_response->values()->where('input_id', $input->id)->first();
            $input->value = $user_response_value == null ? null : $user_response_value->value;
            $input->disabled = $disabled;
            if ($input->limit > 0) {
                $count = 0;
                $responses = Response::where('form_id', $form->id)->get();
                foreach ($responses as $response) {
                    $count += $response->values()->where('input_id', $input->id)->get()->count();
                }
                if ($count >= $input->limit && $input->value == null)
                    $input->disabled = 'disabled';
            }
        }

        return $this->getView($event)
            ->with('editing', true);
    }


    public function update(Request $request, Event $event)
    {
        $request->validate([
            'policy' => 'required'
        ]);
        $user = Auth::user();

        // Get the submitted form
        $form = $event->registrationForm;

        if ($form == null) {
            $this->form_errors->add('form', 'Det finns inget registreringsformulär för det här eventet');
            return $this->show($request, $event);
        } else if (!$form->isRegistrationOpen()) {
            $this->form_errors->add('form', 'Registrering är inte tillgängligt för tillfället');
            return $this->show($request, $event);
        }

        $response = Response::where('form_id', $form->id)->where('user_id', Auth::id())->first();

        if ($response == null) {
            $this->form_errors->add('form', 'Du försöker uppdatera, men du är ännu inte anmäld');
            return $this->show($request, $event);
        }

        // Delete old vaues
        ResponseValue::where('response_id', $response->id)->delete();

        // Iterate through the form responces
        foreach ($request->except(['_token', 'policy', '_method']) as $key => $value) {
            if ($value == NULL)
                continue;

            // Get the input associated with the form response
            $input = FormInput::where('name', $key)->first();

            if ($input == null)
                continue;

            // Create a new form response value
            $response_value = new ResponseValue;
            $response_value->input()->associate($input);
            $response_value->value = $value;

            $response->values()->save($response_value);
        }

        // Create association between user and event
        $barcode_id = 1;
        foreach ($event->users as $eventUser) {
            if ($eventUser->pivot->barcode_id >= $barcode_id) {
                $barcode_id = $eventUser->pivot->barcode_id + 1;
            }
        }

        return redirect(route('home.index'));
    }
}