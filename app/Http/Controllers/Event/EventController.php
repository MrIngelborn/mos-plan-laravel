<?php

namespace App\Http\Controllers\Event;

use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateEvent;
use App\Models\Event;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified'], ['except' => ['show']]);
    }

    public function index()
    {
        // Check for authority
        $this->authorize('list', Event::class);

        $adminbar = null;
        $admin = Auth::user();
        if ($admin) {
            if ($admin->can('create', Event::class))
                $adminbar['Nytt Event'] = route('events.create');
        }

        $table = (object)[];
        $table->headers = [
            'URI', 'Name', 'Start Time', 'End Time'//, 'Delete?'
        ];
        $table->rows = [];

        foreach (Event::get() as $event) {
            $row = [];
            $row[0] = (object)[];
            $row[0]->link = route('events.show', $event);
            $row[0]->value = $event->uri;
            $row[1] = $event->name;
            $row[2] = $event->start_time;
            $row[3] = $event->end_time;
            //$row[4] = (object)[];
            //$row[4]->delete = true;
            //$row[4]->link = route('event.destroy', $event->uri);
            //$row[4]->value = 'Delete';
            $table->rows[] = $row;
        }

        $header = 'Events';

        return view('general.table')
            ->with('adminbar', $adminbar)
            ->with('table', $table)
            ->with('header', $header);
    }

    public function show(Event $event)
    {
        // Create adminbar
        $adminbar = null;
        $admin = Auth::user();
        if ($admin) {
            if ($admin->can('update', $event))
                $adminbar['Redigera Event'] = route('events.edit', $event);
            if ($admin->can('list', [Response::class, $event]))
                $adminbar['Anmälningar'] = route('events.responses.index', $event);
            if ($admin->can('list', [Payment::class, $event]))
                $adminbar['Betalningar'] = route('events.payments.index', $event);
            if ($admin->can('create', [Purchase::class, $event]))
                $adminbar['Shop'] = route('events.shop.show', $event);
            if ($admin->can('create', [Checkin::class, $event]))
                $adminbar['Incheckningsstatus'] = route('events.checkin.index', $event);
            if ($admin->can('list', [Purchase::class, $event])) {
                $adminbar['Ekonomi'] = route('events.economy', $event);
                $adminbar['Användarekonomi'] = route('events.user_economy', $event);
            }
            if ($admin->can('view', [Event\Registration\Form::class, $event])) {
                $adminbar[__('event.form')] = route('events.form.show', $event);
            }
        }


        return view('event.event')
            ->with('event', $event)
            ->with('adminbar', $adminbar);
    }

    public function edit(Request $request, Event $event)
    {
        // Check for authority
        $this->authorize('update', $event);

        return view('event.edit')->with('event', $event);
    }

    public function update(UpdateEvent $request, Event $event)
    {
        // Check for authority
        $this->authorize('update', $event);

        $event->uri = $request['uri'];
        $event->name = $request['name'];
        $event->start_time = $request['start_time'];
        $event->end_time = $request['end_time'];
        $event->entry_price = $request['entry_price'];
        $event->max_seats = $request['max_seats'];
        $event->barcode_prefix = $request['barcode_prefix'];
        $event->description = $request['description'];
        $event->max_debt = $request['max_debt'];

        $event->save();


        return redirect(route('events.show', $event));
    }

    /**
    * Show form to create a new Event
    */
    public function create(Event $event)
    {
        // Check for authority
        $this->authorize('create', Event::class);

        $title = 'Nytt Event';

        $form = (object)[];
        $form->action = route('events.store');
        $form->inputs = [];

        $form->inputs[] = (object)[
            'name' => 'uri',
            'label' => 'URI',
            'type' => 'text',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'name',
            'label' => 'Namn',
            'type' => 'text',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'start_time',
            'label' => 'Starttid',
            'type' => 'datetime-local',
            'max' => '9999-12-31T23:59:59',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'end_time',
            'label' => 'Sluttid',
            'type' => 'datetime-local',
            'max' => '9999-12-31T23:59:59',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'entry_price',
            'label' => 'Entrépris',
            'type' => 'number',
            'min' => '0',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'barcode_prefix',
            'label' => 'Prefix Streckkod',
            'type' => 'text',
            'required' => true,
        ];
        $form->inputs[] = (object)[
            'name' => 'max_debt',
            'label' => 'Maximal skuld',
            'type' => 'number',
            'required' => false,
        ];
        $form->inputs[] = (object)[
            'name' => 'description',
            'label' => 'Beskrivning',
            'type' => 'textarea',
            'required' => false,
        ];

        $form->inputs[] = (object)[
            'name' => '',
            'type' => 'submit',
            'value' => 'Skapa',
        ];

        return view('general.form')
            ->with('title', $title)
            ->with('form', $form);
    }

    public function store(Request $request)
    {
        // Check for authority
        $this->authorize('create', Event::class);

        $validated = $request->validate([
            'uri' => [
                'required',
                'max:20',
                'alpha_num',
                Rule::unique('events'),
            ],
            'name' => 'required|max:100',
            'start_time' => 'required|date',
            'end_time' => 'required|date|after:start_time',
            'entry_price' => 'required|integer|min:0',
            'barcode_prefix' => 'required|min:1|max:100',
            'max_debt' => 'nullable|min:0',
            'description' => 'string|nullable',
        ]);

        Event::create($validated);

        return redirect(route('events.index'));
    }
}
