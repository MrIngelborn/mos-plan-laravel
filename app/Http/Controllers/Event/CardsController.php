<?php

namespace App\Http\Controllers\Event;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Event;

class CardsController extends Controller
{
    
    public function __invoke(Request $request, Event $event)
    {
        // Auth
        $this->authorize('list', [Response::class, $event]);
        
        $card_count = intval($request->input('cards', 0));
        
        // Get form and responses
        $form = $event->registrationForm;
        $responses = $form->responses;
        
        // Get the inputs which should be display on card
        $inputs_boxes = $form->inputs()->where('type', 'checkbox')->where('oncard', true)->get();
        
        // Track max barcode
        $max_barcode_id = 0;
        
        $cards = [];
        
        // Create cards for each of the responses
        foreach ($responses as $response) {
            $user = $response->user;
            
            $event_relation = $user->events()->where('event_id', $event->id)->first();
            if ($event_relation == null) continue; // Should not happen
            
            $barcode_id = $event_relation->pivot->barcode_id;
            $barcode = $event->barcode_prefix . sprintf('%03d', $barcode_id);
            $max_barcode_id = $barcode_id > $max_barcode_id ? $barcode_id : $max_barcode_id;
            
            $boxes = [];
            
            foreach ($inputs_boxes as $input) {
                $response_value = $response->values()->where('input_id', $input->id)->first();
                $box = (object)[];
                $box->checked = $response_value != null ? $response_value->value == 'on' ? true : false : false;
                $box->label = $input->label;
                $boxes[] = $box;
            }
            
            $card = (object)[];
            
            $card->username = $user->name;
            $card->barcode = $barcode;
            $card->boxes = $boxes;
            
            $cards[] = $card;
            $card_count--;
        }
        
        $barcode_id = $max_barcode_id+1;
        
        while ($card_count > 0) {
            $boxes = [];
            
            $barcode = $event->barcode_prefix . sprintf('%03d', $barcode_id++);
            
            foreach ($inputs_boxes as $input) {
                $response_value = $response->values()->where('input_id', $input->id)->first();
                $box = (object)[];
                $box->checked = false;
                $box->label = $input->label;
                $boxes[] = $box;
            }
            
            $card = (object)[];
            
            $card->username = "_";
            $card->barcode = $barcode;
            $card->boxes = $boxes;
            
            $cards[] = $card;
            $card_count--;
        }
        
        //var_dump($cards);
        
        return view('event.cards')
            ->with('cards', $cards);
    }
    
}
