<?php

namespace App\Http\Controllers\Event;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

use App\Http\Controllers\Controller;
use App\Models\Checkin;
use App\Models\Event;
use App\Models\ViewCheckinStatus;
use App\Models\ViewUserEconomy;

class CheckinController extends Controller
{
    
    public function index(Event $event)
    {
        // Auth
        $this->authorize('list', [Checkin::class, $event]);
        
        $checkin_statuses = ViewCheckinStatus::where('event_id', $event->id)->get();
        
        $table = (object)[];
        $table->headers = ['Användare', 'Status', 'Senast ändrad'];
        $table->rows = [];
        
        foreach ($checkin_statuses as $status) {
            $row = [];
            $row[] = $status->user->name;
            $row[] = $status->check_in ? 'Inckeckad' : 'Utcheckad';
            $row[] = $status->time;
            $table->rows[] = $row;
        }
        
        $header = "Incheckningsstatus för {$event->name}";
        
        $adminbar = [];
        if (Auth::user()->can('create', [Checkin::class, $event]))
            $adminbar['Incheckining'] = route('events.checkin.create', $event);
        
        return view('general.table')
            ->with('table', $table)
            ->with('header', $header)
            ->with('adminbar', $adminbar);
    }
    
    public function create(Event $event)
    {
        // Auth
        $this->authorize('create', [Checkin::class, $event]);
        
        return view('event.checkin')
            ->with('event', $event);
    }
    
    public function store(Request $request, Event $event)
    {
        // Auth
        $this->authorize('create', [Checkin::class, $event]);
        
        $prefix = preg_quote($event->barcode_prefix);
        
        // Validate request
        $validated = $request->validate([
            'barcode' => "regex:/^(?i)$prefix([0-9]+)/"
        ]);
        
        preg_match("/^(?i)$prefix([0-9]+)/", $validated['barcode'], $matches);
        
        // Get user based on barcode
        $user = $event->users()->wherePivot('barcode_id', $matches[1])->first();
        
        if (! $user) {
            $errors = new MessageBag();
            $errors->add('barcode', 'User not found');
            return view('event.checkin')
                ->with('event', $event)
                ->withErrors($errors);
        }
        
        $status = Checkin::getStatus($event, $user);
        $new_status = ($status+1) % 2;
        
        // Create new event
        $checkin = new Checkin;
        $checkin->user()->associate($user);
        $checkin->event()->associate($event);
        $checkin->check_in = $new_status;
        $checkin->save();
        
        // Get debt
        $debt = ViewUserEconomy::where([
                'user_id' =>  $user->id,
                'event_id' => $event->id
        ])->first()->debt;
        
        
        return view('event.checkin')
            ->with('event', $event)
            ->with('user', $user)
            ->with('first_checkin', $status===null)
            ->with('status', $new_status)
            ->with('debt' , $debt);
    }
}
