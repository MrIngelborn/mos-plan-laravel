<?php

namespace App\Http\Controllers\Event\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Product;

class BarcodeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, Event $event)
    {
        // Authorize
        $this->authorize('list', [Product::class, $event]);
        
        $products = Product::where('event_id', $event->id)->get();
        
        $barcodes = [
            (object)[
                'title' => 'Töm varukorg',
                'barcode' => 'clear'
            ],
            (object)[
                'title' => 'Ta bort en',
                'barcode' => '-'
            ],
        ];
        
        return view('event.shop.barcodes')
            ->with('barcodes', $barcodes)
            ->with('products', $products);
    }
}
