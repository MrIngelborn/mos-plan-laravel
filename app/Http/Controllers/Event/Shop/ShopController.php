<?php
namespace App\Http\Controllers\Event\Shop;

use App\Models\Purchase;
use App\Models\Product;
use App\Models\Event;
use App\Models\User;
use App\Models\ViewUserEconomy;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    private const SESSION_VAR_CART = 'cart_items';
    private const SESSION_VAR_MOD_DELETE = 'cart_mod_delete';
    
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function show(Request $request, Event $event)
    {
        // Authorize request
        $this->authorize('create', [Purchase::class, $event]);
        
        $cart = $this->getCart($request, $event);
        
        $cart_value = 0;
        if ($cart) foreach ($cart as $product_id => $amount) {
            $cart_value += Product::find($product_id)->price * $amount;
        }
        
        if ($cart) $cart = collect($cart)->map(function($value, $key) {
            $product = Product::find($key);
            return $product ? (object) [
                'name' => $product->name,
                'price' => $product->price,
                'amount' => $value,
                'total_price' => $value * $product->price
            ] : null;
        });
        
        $adminbar = [];
        if (Auth::user()->can('list', [Product::class, $event]))
            $adminbar['Produkter'] = route('events.shop.products.index', $event);
        
        return view('event.shop.shop')
                ->with('cart', $cart)
                ->with('sum', $cart_value)
                ->with('adminbar', $adminbar);
    }
    
    public function enterBarcode(Request $request, Event $event)
    {
        // Authorize request
        $this->authorize('create', [Purchase::class, $event]);
        
        // Validate request
        $validator = $request->validate([
            'barcode' => 'required'
        ]);
        
        $barcode = $request->barcode;
        $notice = (object)[];
        
        // Test for different type of commands
        switch ($barcode) {
            case 'clear':
                $this->clearCart($request, $event);
                $notice->color = 'yellow';
                $notice->message = "Tömde varukorgen";
                break;
            case '-':
            case '+':
                $request->session()->flash(self::SESSION_VAR_MOD_DELETE, true);
                $notice->color = 'yellow';
                $notice->message = "Tar bort en av nästa produkt";
                break;
            default:
                $product = $event->products()->where('barcode', $barcode)->first();
                if ($product) {
                    $this->addProduct($request, $event, $product);
                    $notice->color = 'green';
                    $notice->message = $request->session()->has(self::SESSION_VAR_MOD_DELETE) ? 'Tog bort' : 'La till';
                    $notice->message .= " $product->name";
                    break;
                }
                
                $prefix = preg_quote($event->barcode_prefix);
                
                preg_match("/$prefix([0-9]+)/", $barcode, $matches);
                
                if (isset($matches[1])) {
                    // Match found, check if user is found
                    $user = $event->users()
                            ->wherePivot('barcode_id', '=', $matches[1])
                            ->first();
                    if ($user) {
                        $result = $this->checkoutCart($request, $event, $user);
                        $notice->color = $result[0];
                        $notice->message = $result[1];
                    }
                    else {
                        $notice->color = 'red';
                        $notice->message = 'Användaren hittades inte';
                    }
                }
                else {
                    $notice->color = 'red';
                    $notice->message = 'Hittade inte produkten';
                }
        }
        
        return $this->show($request, $event)
            ->with('notice', $notice);
    }
    
    private function checkoutCart(Request $request, Event $event, User $user)
    {
        $cart = $this->getCart($request, $event);
        
        if (empty($cart)) return ['red', 'Lägg till produkter i varukorgen innan du avslutar ditt köp'];
        
        // Check that puchase doesnt exceed max debt
        $max_debt = $event->max_debt;
        $debt = ViewUserEconomy::where([
                'user_id' =>  $user->id,
                'event_id' => $event->id
        ])->first()->debt;
        $cart_value = 0;
        foreach ($cart as $product_id => $amount) {
            $cart_value += Product::find($product_id)->price * $amount;
        }
        if ($debt + $cart_value > $max_debt) {
            return ['red', "Köpet ej giltigt! Din ({$user->name}) skuld ($debt) är över maxskulden ($max_debt). Betala av din skuld hos ansvarig funktionär innan du handlar mer"];
        }
        
        $purchase = new Purchase;
        $purchase->user_id = $user->id;
        $purchase->event_id = $event->id;
        $purchase->save();
        
        // Add products to purchase
        foreach ($cart as $product_id => $amount) {
            $purchase->products()->attach($product_id, ['amount' => $amount]);
        }
        
        $this->clearCart($request, $event);
        return ['green', 'Köp godkänt, välkommen åter!'];
    }
    
    private function addProduct(Request $request, Event $event, Product $product)
    {
        $id = $product->id;
        
        $cart = $this->getCart($request, $event);
        
        $change = $request->session()->has(self::SESSION_VAR_MOD_DELETE) ? -1 : 1;
        $cart[$id] = isset($cart[$id]) ? $cart[$id]+$change : 1;
        if ($cart[$id] <= 0) unset($cart[$id]);
        
        $this->setCart($request, $event, $cart);
    }
     
    private function getCart(Request $request, Event $event) {
        return $request->session()->get(self::SESSION_VAR_CART."_{$event->id}");
    }
    
    private function setCart(Request $request, Event $event, $cart) {
        return $request->session()->put(self::SESSION_VAR_CART."_{$event->id}", $cart);
    }
    
    private function clearCart(Request $request, Event $event)
    {
        return $request->session()->forget(self::SESSION_VAR_CART."_{$event->id}");
    }
}