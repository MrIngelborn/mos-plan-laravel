<?php

namespace App\Http\Controllers\Event\Shop;

use Validator;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Product;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index(Request $request, Event $event)
    {
        // Authorize
        $this->authorize('list', [Product::class, $event]);
        
        $header = "Produktlista för {$event->name}";
        
        $table = (object)[];
        $table->headers = [
            'id', 'barcode', 'name', 'price', 'Delete?'
        ];
        
        $table->rows = [];
        foreach ($event->products as $product) {
            $row = [];
            
            $row['id'] = (object)[];
            $row['id']->value = $product->id;
            $row['id']->link = route('events.shop.products.edit', [$event, $product]);
            
            $row['barcode'] = $product->barcode;
            $row['name'] = $product->name;
            $row['price'] = $product->price;
            
            if(Auth::user()->can('delete', $product)) {
                $row['delete'] = (object)[];
                $row['delete']->delete = true;
                $row['delete']->link = route('events.shop.products.destroy', [$event, $product]);
                $row['delete']->value = 'Delete';
            }
            else $row['delete'] = '-';
                       
            $table->rows[] = $row;
        }
        
        $adminbar = [
            'Streckkoder' => route('events.shop.barcodes', $event),
        ];
        if (Auth::user()->can('create', [Product::class, $event]))
            $adminbar['Ny produkt'] = route('events.shop.products.create', $event);
        
        return view('general.table')
            ->with('header', $header)
            ->with('table', $table)
            ->with('adminbar', $adminbar);
    }
    
    public function create(Request $request, Event $event)
    {
        // Authorize
        $this->authorize('create', [Product::class, $event]);
        
        $action = route('events.shop.products.store', $event);
        
        return view('event.shop.product_new')
            ->with('action', $action);
    }
    
    public function store(Request $request, Event $event)
    {
        // Authorize
        $this->authorize('create', [Product::class, $event]);
        
        $validated = $request->validate([
            'barcode' => [
                'required',
                Rule::unique('products')->where(function ($query) use ($event) {
                    return $query->where('event_id', $event->id);
                })  
            ],
            'name' => 'required|string',
            'price' => 'required|integer'
        ]);
        
        $product = new Product();
        $product->event()->associate($event);
        $product->name = $validated['name'];
        $product->price = $validated['price'];
        $product->barcode = $validated['barcode'];
        $product->save();
        
        return redirect(route('events.shop.products.index', $event));
    }
    
    public function show(Request $request, Event $event, Product $product)
    {
        // Authorize
        $this->authorize('view', $product);
        
        $header = "Editera produkt";
        
        return view('event.shop.product')
            ->with('header', $header)
            ->with('product', $product);
    }
    
    public function edit(Request $request, Event $event, Product $product)
    {
        return redirect(route('events.shop.products.show', [$event, $product]));
    }
    
    public function update(Request $request, Event $event, Product $product)
    {
        // Authorize
        $this->authorize('update', $product);
        
        $validated = $request->validate([
            'barcode' => [
                'required',
                Rule::unique('products')->where(function ($query) use ($event) {
                    return $query->where('event_id', $event->id);
                })->ignore($product->id, 'id')  
            ],
            'name' => 'required|string',
            'price' => 'required|integer'
        ]);
        
        $product->name = $validated['name'];
        $product->price = $validated['price'];
        $product->barcode = $validated['barcode'];
        $product->save();
        
        return redirect(route('events.shop.products.index', $event));
    }
    
    public function destroy(Request $request, Event $event, Product $product)
    {
        // Authorize
        $this->authorize('delete', $product);
        
        $product->delete();
        
        return redirect(route('events.shop.products.index', $event));
    }
}
