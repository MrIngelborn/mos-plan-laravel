<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    private function array_to_permisstion_tree(array $array)
    {
        $tree = (object)[];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $tree->children[$key] = (object)[];
                $tree->children[$key] = $this->array_to_permisstion_tree($value);
            }
            else {
                $tree->children[$value] = (object)[];
            }
        }
        return $tree;
    }
    
    private function checkPermission(object &$tree, array $permission)
    {
        /*
        echo PHP_EOL.'permission: ';
        foreach($permission as $subperm) {
            echo $subperm . '.';
        }
        echo PHP_EOL;
        */
        if (!isset($permission[0])) return;
        
        $key = \array_splice($permission, 0, 1)[0];
        //echo 'key: ';
        //var_dump($key);
        switch (count($permission)) {
            case 0:
                //echo 'case 1: ';
                if ($key == '*') {
                    $tree->checked = true;
                    //echo '* checked'.PHP_EOL;
                    return;
                }
                if (array_key_exists($key, $tree->children)) {
                    $tree->children[$key]->checked = true;
                    //echo "$key checked".PHP_EOL;
                }
                else {
                    //echo "$key does not exists";
                    //var_dump($tree->children);
                }
                break;
            default:
                //echo 'default case'.PHP_EOL;
                if (array_key_exists($key, $tree->children)) {
                    $this->checkPermission($tree->children[$key], $permission);
                }
                else {
                    //print('$tree does not hade the child '. $key.': ');
                    //var_dump($tree->children);
                }
        }
    }
    
    private function init_permissions()
    {
        $resource = [
            'list',
            'view',
            'create',
            'update',
            'delete',
        ];
        
        $tree = [
            'event' => $resource,
            'user' => $resource,
            'role' => $resource,
        ];
        
        // Create a list of event_uris
        $event_uris = Event::get()->map(function($event) {
            return $event->uri;
        })->toArray();
        // Add * to the beginning of the uri list
        array_unshift($event_uris, '*');
        
        foreach ($event_uris as $uri) {
            $tree['event'][$uri] = [
                'view',
                'update', 
                'delete',
                'products'  => $resource,
                'purchase'  => $resource,
                'response'  => $resource,
                'form'      => $resource,
                'payment'   => $resource,
                'checkin'   => $resource,
            ];
        }
        
        return $this->array_to_permisstion_tree($tree);
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // Check for authority
        $this->authorize('list', Role::class);
        
        $table = (object)[];
        $table->headers = [
            'Id', 'Name', 'Slug', 'Delete?'
        ];
        $table->rows = [];
        
        foreach (Role::get() as $role) {
            $row = [];
            $row[0] = (object)[];
            $row[0]->link = route('roles.show', $role->id);
            $row[0]->value = $role->id;
            $row[1] = $role->name;
            $row[2] = $role->slug;
            $row[3] = (object)[];
            $row[3]->delete = true;
            $row[3]->link = route('roles.destroy', $role->id);
            $row[3]->value = 'Delete';
            $table->rows[] = $row;
        }
        
        $header = 'Roller';
        
        return view('general.table')
            ->with('table', $table)
            ->with('header', $header);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // Check for authority
        $this->authorize('create', Role::class);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Check for authority
        $this->authorize('create', Role::class);
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        // Check for authority
        $this->authorize('update', $role);
        
        // Create permission tree
        $permissions = $this->init_permissions();
        
        // Mark checked
        foreach ($role->permissions as $permission => $value) {
            $perm_arr = explode('.', $permission);
            $this->checkPermission($permissions, $perm_arr);
        }
        
        return view('role')
            ->with('role', $role)
            ->with('permissions', $permissions);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        // Redirect to show
        return redirect(route('roles.show', $role->id));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role)
    {
        // Check for authority
        $this->authorize('update', $role);
        
        $validated = $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'permissions' => 'array'
        ]);
        
        $role->name = $validated['name'];
        $role->slug = $validated['slug'];
        
        $permissions = [];
        foreach ($validated['permissions'] as $permission) {
            $permissions[$permission] = true;
        }
        
        $role->permissions = $permissions;
        $role->save();
        return redirect(route('roles.index'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        // Check for authority
        $this->authorize('delete', $role);
    }
}
;